Feature: BDD




@TEST0004
Scenario Outline: Node Business Activity 'Error'
When user enters Username: <Username> 
Enters Password: <Password>
And Click on Login button
When Error is performed
Then Error completes
Examples:
	|Username|Password|
	|User2|Pass2|


@TEST0005
Scenario Outline: Node Business Activity 'Open New Account'
When user enters Username: <Username> 
Enters Password: <Password>
And Click on Login button
Then Home Screen displayed
When Open New Account is performed
Then Open New Account completes
Examples:
	|Username|Password|
	|User1|Pass1|


@TEST0006
Scenario Outline: Node Business Activity 'Bill Pay'
When user enters Username: <Username> 
Enters Password: <Password>
And Click on Login button
Then Home Screen displayed
When Bill Pay is performed
Then Bill Pay completes
Examples:
	|Username|Password|
	|User1|Pass1|


