com.conformiq.creator.structure.v15

creator.customaction qml_8a18ae80b4e34f1484bc6b2bdec56e35 "Allocate car"
	interfaces = [ qml_4edf2ecf876c4469b9a6becbed7e15ed ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Allocate car"]

{
}
creator.customaction qml_396fe00d3f53475b917da841450e5460 "_verfication_Car is allocated"
	interfaces = [ qml_e56a1906d2d34574968d443d94bcdfbf ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Car is allocated has completed "]

{
}
creator.customaction qml_1a0e087d931e46de92f07929d9a7c6a7 "_verfication_Request Rejected"
	interfaces = [ qml_e56a1906d2d34574968d443d94bcdfbf ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Request Rejected has completed "]

{
}
creator.externalinterface qml_4edf2ecf876c4469b9a6becbed7e15ed "User"
direction = in;

creator.customaction qml_2298c50bff6744648b1ed35609389011 "_verfication_Allocate car"
	interfaces = [ qml_e56a1906d2d34574968d443d94bcdfbf ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Allocate car has completed "]

{
}
creator.customaction qml_ab564ddd318a4ebf81959c797b85b333 "_verfication_Review Request"
	interfaces = [ qml_e56a1906d2d34574968d443d94bcdfbf ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Review Request has completed "]

{
}
creator.customaction qml_d36d5e5c816940fd98a746937a271583 "_verfication_Platform Transportation"
	interfaces = [ qml_e56a1906d2d34574968d443d94bcdfbf ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Platform Transportation has completed "]

{
}
creator.customaction qml_b040576e5c94442b87d135e0e9b7b659 "Platform Transportation"
	interfaces = [ qml_4edf2ecf876c4469b9a6becbed7e15ed ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Platform Transportation"]

{
}
creator.customaction qml_67c40f99b7a74cb9832830b5f521a8b8 "_verfication_Register request"
	interfaces = [ qml_e56a1906d2d34574968d443d94bcdfbf ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Register request has completed "]

{
}
creator.customaction qml_8d3c97bf906b4ce5a8ab8596741f3c26 "Register request"
	interfaces = [ qml_4edf2ecf876c4469b9a6becbed7e15ed ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Register request"]

{
}
creator.customaction qml_86312b9824fd4e4db0a11b833d433429 "Car is allocated"
	interfaces = [ qml_4edf2ecf876c4469b9a6becbed7e15ed ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Car is allocated"]

{
}
creator.customaction qml_bc308ffe73af4ecbb0acf967443dfc22 "Is Option?"
	interfaces = [ qml_4edf2ecf876c4469b9a6becbed7e15ed ]
	shortform = "AI"
	direction = in
	tokens = [ literal "Option is "]

{
}
creator.customaction qml_1abddc7c08e446e8b5c6c7b9012b2988 "Review Request"
	interfaces = [ qml_4edf2ecf876c4469b9a6becbed7e15ed ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Review Request"]

{
}
creator.externalinterface qml_e56a1906d2d34574968d443d94bcdfbf "System"
direction = out;

creator.customaction qml_fac856d3070c46d1b3f981e261950c40 "Request Rejected"
	interfaces = [ qml_4edf2ecf876c4469b9a6becbed7e15ed ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Request Rejected"]

{
}
