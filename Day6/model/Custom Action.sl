com.conformiq.creator.structure.v15
creator.customaction qml800fc4bac6e04466b1c87d92730cad52 "RightClick"
	interfaces = [ qml3554dfe29cd54a3a94e110c0c62a21f3 ]
	shortform = "RC"
	direction = in
	tokens = [ literal "Click on " reference qml646835af2be8449985600885f3cb6f5d
literal "Button" ]
{
	creator.primitivefield qml646835af2be8449985600885f3cb6f5d "Button Name"
		type = String;
}
creator.externalinterface qml3554dfe29cd54a3a94e110c0c62a21f3 "User123"
	direction = in;
creator.customaction qml622100c087f74815a69c436da7df5b25 "Verify Status"
	interfaces = [ qmlc7d2fd098d75418cbbfe02c8a91acc4b ]
	shortform = "VS"
	direction = out
	tokens = [ literal "Verify status as : " reference
qmlbe6c14e0b3b5421aaebed8054d99dbf6 ]
{
	creator.primitivefield qmlbe6c14e0b3b5421aaebed8054d99dbf6 "Status"
		type = String;
}
creator.externalinterface qmlc7d2fd098d75418cbbfe02c8a91acc4b "Display123"
	direction = out;