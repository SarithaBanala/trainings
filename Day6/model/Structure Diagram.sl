com.conformiq.creator.structure.v15
creator.gui.screen qml657ba737ad4e44aabbc66604b971eb73 "Login"
{
	creator.gui.button qml9bfdc7ab87c34e37876f9e98c00c56eb "Login"
		status = dontcare;
	creator.gui.form qml580a6f761cb44261bec8b62c619eb594 "Customer Login"
	{
		creator.gui.textbox qml2424df548a1c447196674567bbf9be1f "Username"
			type = String
			status = dontcare;
		creator.gui.textbox qml2cc886170e6a4ae89fa02e01d0c578ff "Password"
			type = String
			status = dontcare;
	}
	creator.gui.labelwidget qmle5ac3a5579ae4ad484891c3a056d4b63 "Status"
		status = dontcare;
	creator.gui.form qml992fe16f170040a0b96aa6cdfc130986 "unnamed"
		deleted
	{
	}
}
creator.gui.screen qml799f0eead238440298b9eef8caf6a7fb "Home"
{
	creator.gui.hyperlink qml91d44c31d5de48ae97719b14c134b080 "Open New Account"
		status = dontcare;
	creator.gui.hyperlink qmlddc8eba7c0f6434d9e2c1bc49b23c333 "Transfer Funds"
		status = dontcare;
	creator.gui.hyperlink qmlb04f4fc9360249a9982209b589d8ef31 "Logout"
		status = dontcare;
	creator.gui.labelwidget qml5e153a7fb022452db1c20b50c3ef3971 "Status"
		status = dontcare;
}
creator.gui.screen qml5d6d91143789435c93b33a8670cc1df2 "Open New Account"
{
	creator.gui.form qmld8cb98287bba4b7a83152c96eccde64f "Open New Account"
	{
		creator.gui.dropdown qmleee1626abaaf42d293ea7cf93f39fd67 "Account Type"
			type = qmlff496ed971144f4489f7122a23a73edf
			status = dontcare;
		creator.gui.dropdown qml8b674cfc6e02432fafc13430a1904b28 "Account Number"
			type = qml6823d4016dd0421995442e32843216b4
			status = dontcare;
	}
	creator.gui.button qmlca7c45bc2bfd48cf80d59570b4a88c95 "Open New Account"
		status = dontcare;
	creator.gui.labelwidget qml6827ebd8e68940799cb5d963ca5b145b "Status"
		status = dontcare;
}
creator.enum qmlff496ed971144f4489f7122a23a73edf "Account Type"
{
	creator.enumerationvalue qmlb463d9a902484637b82f479367522e61 "Checking";
	creator.enumerationvalue qmlbe0248da0a2e4ad4b46d37d7dbf8b0f7 "Savings";
}
creator.enum qml6823d4016dd0421995442e32843216b4 "Account Number"
{
	creator.enumerationvalue qml66062adcfeee47fa904c8beaa7b1d091 "12444";
	creator.enumerationvalue qml1bba20421bbe4dfebbb199668d7b96d2 "23546";
}
creator.gui.screen qml64dfa39e7f2d4573947741237cdd064f "Transfer Funds"
{
	creator.gui.form qmla819126dbe6d4dd685255f03bd000f99 "Transfer Funds"
	{
		creator.gui.textbox qmlfe9c63764c58464c89fcb9867915f4e0 "Amount"
			type = String
			status = dontcare;
		creator.gui.dropdown qml0a20fc5fa0234a30b1cbd040d4a73cff "From Account"
			type = qml6823d4016dd0421995442e32843216b4
			status = dontcare;
		creator.gui.dropdown qml25a546fbf6f346e888502ef497e1bdb6 "To Account"
			type = qml6823d4016dd0421995442e32843216b4
			status = dontcare;
	}
	creator.gui.button qmlcedd167d22314c648128ab0b258642e0 "Transfer"
		status = dontcare;
}
creator.message qml3daa70399ad04b019c5b88d1998f2df0 "User Info Request"
	interfaces = [ qml164066e681c7470d95f29696ac4c53a1 ]
{
	creator.primitivefield qml9e4c1d53a32d4120a5979ecd0ead6a5f "Name"
		type = String;
	creator.enumerationfield qml386465615b6f4717ae58d6029fa45773 "Gender"
		type = qml97d18c28631e4c139b338276e5fcfbab;
	creator.structuredfield qml81d6a3bb833c4d8d80169b9302732057 "Address"
		type = qml7c1ae16de5e749c2a2dc9e6078d863a4;
}
creator.externalinterface qml164066e681c7470d95f29696ac4c53a1 "User1"
	direction = in;
creator.enum qml97d18c28631e4c139b338276e5fcfbab "Gender1"
{
	creator.enumerationvalue qml296afd21e7334bbd9e290aceb34aa0b5 "M";
	creator.enumerationvalue qmlb0ae9fef27404c64911133f910da4bf1 "F";
}
creator.sequencetype qml430dad7fd37d4f149e71735d78d2b833 "Address"
{
	creator.primitivefield qmlb24863e915394f729c60b169422b0402 "S.No"
		type = String;
	creator.primitivefield qml4a00aef1d58049e6bab4191a10abb89c "City"
		type = String;
	creator.primitivefield qmld9ec6f8bf0bf499aaf8def17df7be0d8 "Country"
		type = String;
	creator.primitivefield qml73f7e1d03aa345809168a29f2dc98265 "ZipCode"
		type = String;
}
creator.sequencetype qml7c1ae16de5e749c2a2dc9e6078d863a4 "Address1"
{
	creator.structuredfield qmle33ed11e717140ca9d13824be39f8e46
	"Permanent Address"
		type = qml430dad7fd37d4f149e71735d78d2b833;
	creator.primitivefield qml332db663da294620933ac768514f0776 "unnamed"
		type = String
		deleted;
	creator.structuredfield qmlb799e23f1e364024883b7289e81b0cba "Temp Address"
		type = qml430dad7fd37d4f149e71735d78d2b833;
}
creator.datatable qml77c271469f664438a99a6076281088aa "unnamed"
	deleted
{
	creator.primitivecell qmlbb53e3056de54d7aa0d03e5c840065e2 "unnamed"
		type = String
		deleted;
}
creator.gui.screen qml3eb9a2432db046608ba9428eaaede78c "Make My Trip"
{
	creator.gui.uitable qml03a0b020fd5c4061b31e78a70cefeb0c "Flight details"
	datatable = qml3db34221f3074eb78280ab1360f9bd6c
	{
		creator.gui.button qmlefd36f682a784c20847d529f8bd9c734 "Select"
			status = dontcare;
		creator.gui.hyperlink qml26531a4688684641951140cff8d3b4d0 "Flight Details"
			status = dontcare;
	}
}
creator.datatable qml3db34221f3074eb78280ab1360f9bd6c "Flight Info"
{
	creator.primitivecell qml66c494e2fe6e4a57991b45538e058cef "Flight Name"
		type = String;
	creator.primitivecell qmld93e8fc1fb114a04bbea67c2849503eb "Src"
		type = String;
	creator.primitivecell qml564ea550cf4048c19a0ade31f4b6f263 "Destination"
		type = String;
	creator.primitivecell qmlfd1482e469aa4499b6a0466510f973e8 "From Time"
		type = String;
	creator.primitivecell qml1c70bacd81f045fb822bd54fb6955fed "To Time"
		type = String;
	creator.primitivecell qmle44c713647344eb981a8ff6a3bd6e880 "Price"
		type = String;
}
creator.gui.screen qmlcd623c389e6848dc9c1f9de3a8f3a278 "Order deatils"
{
	creator.gui.form qml4740f2bfb605430593dda60b252aef13 "Ord Info"
	{
		creator.gui.textbox qml2784dbba8fc14832ad3276f6ece996d0 "OrdName"
			type = String
			status = dontcare;
		creator.gui.textbox qmlfd255a589cf24c2f97c752c4dad3a57f "Qty"
			type = String
			status = dontcare;
	}
}