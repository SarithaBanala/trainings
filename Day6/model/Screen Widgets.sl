com.conformiq.creator.structure.v15
creator.gui.screen qml44d51e185da346af88c0005e54f51b7e "Screen"
{
	creator.gui.button qml3d1cb9e31f854187bf2acab80ea10070 "Btn"
		status = dontcare;
	creator.gui.hyperlink qml6b89a2b590954ff29b009317b1e6ebca "Link"
		status = dontcare;
	creator.gui.form qml053638dbf2e44e64968d54c5ffcd437c "Form"
	{
		creator.gui.button qmlffc7bb02548d4c9199d56ce81f935c02 "Btn"
			status = dontcare;
		creator.gui.hyperlink qmlbc9891bf21bf4456a96f81e66d71fe08 "Link"
			status = dontcare;
		creator.gui.labelwidget qmlb6862c6c61ee4de78710dcb9ee9624be "Label"
			status = dontcare;
		creator.gui.textbox qml959cd8bfbfeb497d80f013b894f6c842 "TB"
			type = String
			status = dontcare;
		creator.gui.checkbox qml892eef10987749a3b53fbad72ef190a4 "CB"
			status = dontcare
			checked = dontcare;
		creator.gui.dropdown qml76685c693fd34c318c7acc384d6414d6 "DD"
			type = qml73520b864a1c4ded8a6c4d7e1b26639e
			status = dontcare;
		creator.gui.radiobutton qml76baefe52754450898cb753314fc5a4e "RB"
			type = qml73520b864a1c4ded8a6c4d7e1b26639e
			status = dontcare;
		creator.gui.calendar qml3d586ae73d0d400faa55123e96e48c5f "Calendar"
			status = dontcare;
		creator.gui.listbox qmlbea53e2cec6042abb0ce1820150d9a51 "LB"
			status = dontcare
			items = [ ];
	}
	creator.gui.labelwidget qml253e46c2b14c48bea06fe56cea10e48a "Label"
		status = dontcare;
	creator.gui.menubar qml46e5617f37cc46c89088a3f79a69bb41 "Menus"
	{
		creator.gui.menu qml61961f93b9d2482c9dc23aee9cb4c385 "File"
		{
			creator.gui.clickchoice qml2b1824e879ac435eb90d2233bb6c5c39 "Click";
			creator.gui.mouseoverchoice qmlc1cad13f2bf14fa1a0f86612ca89eccb
			"Mouse over";
			creator.gui.menu qml03a5e84b737b4813ab28d7853e3314ea "SubMenus"
			{
			}
		}
	}
	creator.gui.tab qmlacb490530dae4afa8ffe7158fd652e95 "Modeling"
		selected
	{
	}
	creator.gui.tab qml496a946617ab476f9d2a2b807fc55d9e "Test Design"
	{
	}
	creator.gui.group qmla1b3c9e7a2144cc7a4b91de566b156b3 "unnamed"
	{
	}
	creator.gui.treenode qml5653e58820f24779ada94d76fa88cb73 "Project explorer"
	{
		creator.gui.treenode qmlfea1a454d3ed4a5298e0db5b1aa7bf3f "Day1"
		{
			creator.gui.treenode qmle0adc4f601704f48a3e6e09e11a64d4f "DC"
			{
				creator.gui.treenode qmlc14da9ee91044017b0155f37fa95318d "ExcelScripter"
				{
				}
			}
			creator.gui.treenode qmle5506b125d114036b75600b4c0692f7d
			"ConformiQ Options"
			{
			}
			creator.gui.treenode qmldb3751473d46416e8898906e86bf7821
			"Requirement Providers"
			{
			}
			creator.gui.treenode qmlcca7b38eb22a4bdc8519632a25358d2a "Model"
			{
				creator.gui.treenode qmle600dbf506f44b58b38da4eb5f4f8726 "AD"
				{
				}
				creator.gui.treenode qml162973f653fe416d8082bfa981de569d "SD"
				{
				}
				creator.gui.treenode qml7893202ad1aa4716b8351377d49d2b68 ".sl"
				{
				}
			}
		}
	}
}
creator.enum qml73520b864a1c4ded8a6c4d7e1b26639e "Gender"
{
	creator.enumerationvalue qml680ae28d1cb54eb2b6c4f0529610ba30 "F";
	creator.enumerationvalue qml0de1fff829a441019ea247b7d5d5490f "M";
}
creator.gui.popup qmlf9addb71d5b24eb399ace37cc7b7c5c9 "Popup"
{
}