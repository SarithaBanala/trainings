com.conformiq.creator.structure.v15
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_initializeDB__0
"SOAP Envelope for initializeDB"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__initializeDB_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_initializeDB__0
		optional;
	creator.structuredfield
	qml_wsdl_import__initializeDB_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_initializeDB__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_initializeDB__0
"SOAP Header for initializeDB"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_initializeDB__0
"SOAP Body for initializeDB"
{
	creator.structuredfield
	qml_wsdl_import__initializeDB_32_body__sfield__initializeDB__0 "initializeDB"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__initializeDB__0;
}
creator.sequencetype qml_wsdl_import__seq__initializeDB__0 "initializeDB"
	namespace = "http://service.parabank.parasoft.com/"
{
}
creator.externalinterface
qml_wsdl_import__port__ParaBankServiceImplPort__in__0
"ParaBankServiceImplPort_in"
	annotations = [ "wsdl:port" = "ParaBankServiceImplPort";
"soap:address" =
"http://parabank.conformiq.com/parabank/services/ParaBank;jsessionid=FABF82DA743522AA997C97A82074E7BE";
"soap:version" = "1.1";
]
	direction = in;
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_initializeDBResponse__0
"SOAP Envelope for initializeDBResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__initializeDBResponse_32_message__sfield__Header__0 "Header"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_initializeDBResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__initializeDBResponse_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_initializeDBResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_initializeDBResponse__0
"SOAP Header for initializeDBResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_initializeDBResponse__0
"SOAP Body for initializeDBResponse"
{
	creator.structuredfield
	qml_wsdl_import__initializeDBResponse_32_body__sfield__initializeDBResponse__0
	"initializeDBResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__initializeDBResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__initializeDBResponse__0
"initializeDBResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
}
creator.externalinterface
qml_wsdl_import__port__ParaBankServiceImplPort__out__0
"ParaBankServiceImplPort_out"
	annotations = [ "wsdl:port" = "ParaBankServiceImplPort";
"soap:address" =
"http://parabank.conformiq.com/parabank/services/ParaBank;jsessionid=FABF82DA743522AA997C97A82074E7BE";
"soap:version" = "1.1";
]
	direction = out;
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactionsOnDate__0
"SOAP Envelope for getTransactionsOnDate"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsOnDate_32_message__sfield__Header__0 "Header"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsOnDate__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactionsOnDate_32_message__sfield__Body__0 "Body"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsOnDate__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsOnDate__0
"SOAP Header for getTransactionsOnDate"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsOnDate__0
"SOAP Body for getTransactionsOnDate"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsOnDate_32_body__sfield__getTransactionsOnDate__0
	"getTransactionsOnDate"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactionsOnDate__0;
}
creator.sequencetype qml_wsdl_import__seq__getTransactionsOnDate__0
"getTransactionsOnDate"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield
	qml_wsdl_import__getTransactionsOnDate__pfield__accountId__0 "accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield
	qml_wsdl_import__getTransactionsOnDate__pfield__onDate__0 "onDate"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactionsOnDateResponse__0
"SOAP Envelope for getTransactionsOnDateResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsOnDateResponse_32_message__sfield__Header__0
	"Header"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsOnDateResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactionsOnDateResponse_32_message__sfield__Body__0
	"Body"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsOnDateResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsOnDateResponse__0
"SOAP Header for getTransactionsOnDateResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsOnDateResponse__0
"SOAP Body for getTransactionsOnDateResponse"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsOnDateResponse_32_body__sfield__getTransactionsOnDateResponse__0
	"getTransactionsOnDateResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactionsOnDateResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__getTransactionsOnDateResponse__0
"getTransactionsOnDateResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsOnDateResponse__sfield__transaction__0
	"transaction"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__transaction__0 array;
}
creator.sequencetype qml_wsdl_import__seq__transaction__0 "transaction"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__transaction__pfield__id__0 "id"
		type = number;
	creator.primitivefield qml_wsdl_import__transaction__pfield__accountId__0
	"accountId"
		type = number;
	creator.enumerationfield qml_wsdl_import__transaction__enumfield__type__0
	"type"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = qml_wsdl_import__enum__transactionType__0
		optional;
	creator.primitivefield qml_wsdl_import__transaction__pfield__date__0 "date"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__transaction__pfield__amount__0
	"amount"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__transaction__pfield__description__0
	"description"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional;
}
creator.enum qml_wsdl_import__enum__transactionType__0 "transactionType"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.enumerationvalue qml_wsdl_import__transactionType__enumval__Credit__0
	"Credit";
	creator.enumerationvalue qml_wsdl_import__transactionType__enumval__Debit__0
	"Debit";
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_ParaBankServiceException__0
"SOAP Envelope for ParaBankServiceException"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__ParaBankServiceException_32_message__sfield__Header__0
	"Header"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_ParaBankServiceException__0
		optional;
	creator.structuredfield
	qml_wsdl_import__ParaBankServiceException_32_message__sfield__Body__0 "Body"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_ParaBankServiceException__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_ParaBankServiceException__0
"SOAP Header for ParaBankServiceException"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_ParaBankServiceException__0
"SOAP Body for ParaBankServiceException"
{
	creator.structuredfield
	qml_wsdl_import__ParaBankServiceException_32_body__sfield__Fault__0 "Fault"
		type =
	qml_wsdl_import__seq__SOAP_32_Fault_32_for_32_ParaBankServiceException__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Fault_32_for_32_ParaBankServiceException__0
"SOAP Fault for ParaBankServiceException"
{
	creator.primitivefield
	qml_wsdl_import__ParaBankServiceException_32_body__pfield__faultcode__0
	"faultcode"
		type = String;
	creator.primitivefield
	qml_wsdl_import__ParaBankServiceException_32_body__pfield__faultstring__0
	"faultstring"
		type = String;
	creator.primitivefield
	qml_wsdl_import__ParaBankServiceException_32_body__pfield__faultactor__0
	"faultactor"
		type = String
		optional;
	creator.structuredfield
	qml_wsdl_import__ParaBankServiceException_32_body__sfield__detail__0 "detail"
		type =
	qml_wsdl_import__seq__SOAP_32_Fault_32_Detail_32_for_32_ParaBankServiceException__0
		optional;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Fault_32_Detail_32_for_32_ParaBankServiceException__0
"SOAP Fault Detail for ParaBankServiceException"
{
	creator.structuredfield
	qml_wsdl_import__ParaBankServiceException_32_fault__sfield__ParaBankServiceException__0
	"ParaBankServiceException"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__ParaBankServiceException__0;
}
creator.sequencetype qml_wsdl_import__seq__ParaBankServiceException__0
"ParaBankServiceException"
	namespace = "http://service.parabank.parasoft.com/"
{
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getPositionHistory__0
"SOAP Envelope for getPositionHistory"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getPositionHistory_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPositionHistory__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getPositionHistory_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPositionHistory__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPositionHistory__0
"SOAP Header for getPositionHistory"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPositionHistory__0
"SOAP Body for getPositionHistory"
{
	creator.structuredfield
	qml_wsdl_import__getPositionHistory_32_body__sfield__getPositionHistory__0
	"getPositionHistory"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getPositionHistory__0;
}
creator.sequencetype qml_wsdl_import__seq__getPositionHistory__0
"getPositionHistory"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield
	qml_wsdl_import__getPositionHistory__pfield__positionId__0 "positionId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield
	qml_wsdl_import__getPositionHistory__pfield__startDate__0 "startDate"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield
	qml_wsdl_import__getPositionHistory__pfield__endDate__0 "endDate"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getPositionHistoryResponse__0
"SOAP Envelope for getPositionHistoryResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getPositionHistoryResponse_32_message__sfield__Header__0
	"Header"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPositionHistoryResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getPositionHistoryResponse_32_message__sfield__Body__0
	"Body"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPositionHistoryResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPositionHistoryResponse__0
"SOAP Header for getPositionHistoryResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPositionHistoryResponse__0
"SOAP Body for getPositionHistoryResponse"
{
	creator.structuredfield
	qml_wsdl_import__getPositionHistoryResponse_32_body__sfield__getPositionHistoryResponse__0
	"getPositionHistoryResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getPositionHistoryResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__getPositionHistoryResponse__0
"getPositionHistoryResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getPositionHistoryResponse__sfield__historyPoint__0
	"historyPoint"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__historyPoint__0 array;
}
creator.sequencetype qml_wsdl_import__seq__historyPoint__0 "historyPoint"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__historyPoint__pfield__symbol__0
	"symbol"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__historyPoint__pfield__date__0 "date"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__historyPoint__pfield__closingPrice__0
	"closingPrice"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional;
}
creator.message qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_transfer__0
"SOAP Envelope for transfer"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__transfer_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_transfer__0
		optional;
	creator.structuredfield qml_wsdl_import__transfer_32_message__sfield__Body__0
	"Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_transfer__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_transfer__0
"SOAP Header for transfer"
{
}
creator.sequencetype qml_wsdl_import__seq__SOAP_32_Body_32_for_32_transfer__0
"SOAP Body for transfer"
{
	creator.structuredfield
	qml_wsdl_import__transfer_32_body__sfield__transfer__0 "transfer"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__transfer__0;
}
creator.sequencetype qml_wsdl_import__seq__transfer__0 "transfer"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__transfer__pfield__fromAccountId__0
	"fromAccountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__transfer__pfield__toAccountId__0
	"toAccountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__transfer__pfield__amount__0 "amount"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_transferResponse__0
"SOAP Envelope for transferResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__transferResponse_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_transferResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__transferResponse_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_transferResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_transferResponse__0
"SOAP Header for transferResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_transferResponse__0
"SOAP Body for transferResponse"
{
	creator.structuredfield
	qml_wsdl_import__transferResponse_32_body__sfield__transferResponse__0
	"transferResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__transferResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__transferResponse__0
"transferResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield
	qml_wsdl_import__transferResponse__pfield__transferReturn__0 "transferReturn"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransaction__0
"SOAP Envelope for getTransaction"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransaction_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransaction__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransaction_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransaction__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransaction__0
"SOAP Header for getTransaction"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransaction__0
"SOAP Body for getTransaction"
{
	creator.structuredfield
	qml_wsdl_import__getTransaction_32_body__sfield__getTransaction__0
	"getTransaction"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransaction__0;
}
creator.sequencetype qml_wsdl_import__seq__getTransaction__0 "getTransaction"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield
	qml_wsdl_import__getTransaction__pfield__transactionId__0 "transactionId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactionResponse__0
"SOAP Envelope for getTransactionResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactionResponse_32_message__sfield__Header__0
	"Header"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactionResponse_32_message__sfield__Body__0 "Body"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionResponse__0
"SOAP Header for getTransactionResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionResponse__0
"SOAP Body for getTransactionResponse"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionResponse_32_body__sfield__getTransactionResponse__0
	"getTransactionResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactionResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__getTransactionResponse__0
"getTransactionResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionResponse__sfield__transaction__0 "transaction"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__transaction__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_shutdownJmsListener__0
"SOAP Envelope for shutdownJmsListener"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__shutdownJmsListener_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_shutdownJmsListener__0
		optional;
	creator.structuredfield
	qml_wsdl_import__shutdownJmsListener_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_shutdownJmsListener__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_shutdownJmsListener__0
"SOAP Header for shutdownJmsListener"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_shutdownJmsListener__0
"SOAP Body for shutdownJmsListener"
{
	creator.structuredfield
	qml_wsdl_import__shutdownJmsListener_32_body__sfield__shutdownJmsListener__0
	"shutdownJmsListener"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__shutdownJmsListener__0;
}
creator.sequencetype qml_wsdl_import__seq__shutdownJmsListener__0
"shutdownJmsListener"
	namespace = "http://service.parabank.parasoft.com/"
{
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_shutdownJmsListenerResponse__0
"SOAP Envelope for shutdownJmsListenerResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__shutdownJmsListenerResponse_32_message__sfield__Header__0
	"Header"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_shutdownJmsListenerResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__shutdownJmsListenerResponse_32_message__sfield__Body__0
	"Body"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_shutdownJmsListenerResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_shutdownJmsListenerResponse__0
"SOAP Header for shutdownJmsListenerResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_shutdownJmsListenerResponse__0
"SOAP Body for shutdownJmsListenerResponse"
{
	creator.structuredfield
	qml_wsdl_import__shutdownJmsListenerResponse_32_body__sfield__shutdownJmsListenerResponse__0
	"shutdownJmsListenerResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__shutdownJmsListenerResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__shutdownJmsListenerResponse__0
"shutdownJmsListenerResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getCustomer__0
"SOAP Envelope for getCustomer"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getCustomer_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getCustomer__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getCustomer_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getCustomer__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getCustomer__0
"SOAP Header for getCustomer"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getCustomer__0
"SOAP Body for getCustomer"
{
	creator.structuredfield
	qml_wsdl_import__getCustomer_32_body__sfield__getCustomer__0 "getCustomer"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getCustomer__0;
}
creator.sequencetype qml_wsdl_import__seq__getCustomer__0 "getCustomer"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__getCustomer__pfield__customerId__0
	"customerId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getCustomerResponse__0
"SOAP Envelope for getCustomerResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getCustomerResponse_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getCustomerResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getCustomerResponse_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getCustomerResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getCustomerResponse__0
"SOAP Header for getCustomerResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getCustomerResponse__0
"SOAP Body for getCustomerResponse"
{
	creator.structuredfield
	qml_wsdl_import__getCustomerResponse_32_body__sfield__getCustomerResponse__0
	"getCustomerResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getCustomerResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__getCustomerResponse__0
"getCustomerResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getCustomerResponse__sfield__customer__0 "customer"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__customer__0;
}
creator.sequencetype qml_wsdl_import__seq__customer__0 "customer"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__customer__pfield__id__0 "id"
		type = number;
	creator.primitivefield qml_wsdl_import__customer__pfield__firstName__0
	"firstName"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__customer__pfield__lastName__0
	"lastName"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional;
	creator.structuredfield qml_wsdl_import__customer__sfield__address__0
	"address"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = qml_wsdl_import__seq__address__0
		optional;
	creator.primitivefield qml_wsdl_import__customer__pfield__phoneNumber__0
	"phoneNumber"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__customer__pfield__ssn__0 "ssn"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional;
}
creator.sequencetype qml_wsdl_import__seq__address__0 "address"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__address__pfield__street__0 "street"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__address__pfield__city__0 "city"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__address__pfield__state__0 "state"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__address__pfield__zipCode__0 "zipCode"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_updateCustomer__0
"SOAP Envelope for updateCustomer"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__updateCustomer_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_updateCustomer__0
		optional;
	creator.structuredfield
	qml_wsdl_import__updateCustomer_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_updateCustomer__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_updateCustomer__0
"SOAP Header for updateCustomer"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_updateCustomer__0
"SOAP Body for updateCustomer"
{
	creator.structuredfield
	qml_wsdl_import__updateCustomer_32_body__sfield__updateCustomer__0
	"updateCustomer"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__updateCustomer__0;
}
creator.sequencetype qml_wsdl_import__seq__updateCustomer__0 "updateCustomer"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__updateCustomer__pfield__customerId__0
	"customerId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__updateCustomer__pfield__firstName__0
	"firstName"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__updateCustomer__pfield__lastName__0
	"lastName"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__updateCustomer__pfield__street__0
	"street"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__updateCustomer__pfield__city__0
	"city"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__updateCustomer__pfield__state__0
	"state"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__updateCustomer__pfield__zipCode__0
	"zipCode"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield
	qml_wsdl_import__updateCustomer__pfield__phoneNumber__0 "phoneNumber"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__updateCustomer__pfield__ssn__0 "ssn"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__updateCustomer__pfield__username__0
	"username"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__updateCustomer__pfield__password__0
	"password"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_updateCustomerResponse__0
"SOAP Envelope for updateCustomerResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__updateCustomerResponse_32_message__sfield__Header__0
	"Header"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_updateCustomerResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__updateCustomerResponse_32_message__sfield__Body__0 "Body"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_updateCustomerResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_updateCustomerResponse__0
"SOAP Header for updateCustomerResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_updateCustomerResponse__0
"SOAP Body for updateCustomerResponse"
{
	creator.structuredfield
	qml_wsdl_import__updateCustomerResponse_32_body__sfield__updateCustomerResponse__0
	"updateCustomerResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__updateCustomerResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__updateCustomerResponse__0
"updateCustomerResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield
	qml_wsdl_import__updateCustomerResponse__pfield__customerUpdateResult__0
	"customerUpdateResult"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getPosition__0
"SOAP Envelope for getPosition"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getPosition_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPosition__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getPosition_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPosition__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPosition__0
"SOAP Header for getPosition"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPosition__0
"SOAP Body for getPosition"
{
	creator.structuredfield
	qml_wsdl_import__getPosition_32_body__sfield__getPosition__0 "getPosition"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getPosition__0;
}
creator.sequencetype qml_wsdl_import__seq__getPosition__0 "getPosition"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__getPosition__pfield__positionId__0
	"positionId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getPositionResponse__0
"SOAP Envelope for getPositionResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getPositionResponse_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPositionResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getPositionResponse_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPositionResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPositionResponse__0
"SOAP Header for getPositionResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPositionResponse__0
"SOAP Body for getPositionResponse"
{
	creator.structuredfield
	qml_wsdl_import__getPositionResponse_32_body__sfield__getPositionResponse__0
	"getPositionResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getPositionResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__getPositionResponse__0
"getPositionResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getPositionResponse__sfield__position__0 "position"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__position__0;
}
creator.sequencetype qml_wsdl_import__seq__position__0 "position"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__position__pfield__positionId__0
	"positionId"
		type = number;
	creator.primitivefield qml_wsdl_import__position__pfield__customerId__0
	"customerId"
		type = number;
	creator.primitivefield qml_wsdl_import__position__pfield__name__0 "name"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__position__pfield__symbol__0 "symbol"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__position__pfield__shares__0 "shares"
		type = number;
	creator.primitivefield qml_wsdl_import__position__pfield__purchasePrice__0
	"purchasePrice"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_sellPosition__0
"SOAP Envelope for sellPosition"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__sellPosition_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_sellPosition__0
		optional;
	creator.structuredfield
	qml_wsdl_import__sellPosition_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_sellPosition__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_sellPosition__0
"SOAP Header for sellPosition"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_sellPosition__0
"SOAP Body for sellPosition"
{
	creator.structuredfield
	qml_wsdl_import__sellPosition_32_body__sfield__sellPosition__0 "sellPosition"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__sellPosition__0;
}
creator.sequencetype qml_wsdl_import__seq__sellPosition__0 "sellPosition"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__sellPosition__pfield__customerId__0
	"customerId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__sellPosition__pfield__accountId__0
	"accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__sellPosition__pfield__positionId__0
	"positionId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__sellPosition__pfield__shares__0
	"shares"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield
	qml_wsdl_import__sellPosition__pfield__pricePerShare__0 "pricePerShare"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_sellPositionResponse__0
"SOAP Envelope for sellPositionResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__sellPositionResponse_32_message__sfield__Header__0 "Header"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_sellPositionResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__sellPositionResponse_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_sellPositionResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_sellPositionResponse__0
"SOAP Header for sellPositionResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_sellPositionResponse__0
"SOAP Body for sellPositionResponse"
{
	creator.structuredfield
	qml_wsdl_import__sellPositionResponse_32_body__sfield__sellPositionResponse__0
	"sellPositionResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__sellPositionResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__sellPositionResponse__0
"sellPositionResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__sellPositionResponse__sfield__position__0 "position"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__position__0 array;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_createAccount__0
"SOAP Envelope for createAccount"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__createAccount_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_createAccount__0
		optional;
	creator.structuredfield
	qml_wsdl_import__createAccount_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_createAccount__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_createAccount__0
"SOAP Header for createAccount"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_createAccount__0
"SOAP Body for createAccount"
{
	creator.structuredfield
	qml_wsdl_import__createAccount_32_body__sfield__createAccount__0
	"createAccount"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__createAccount__0;
}
creator.sequencetype qml_wsdl_import__seq__createAccount__0 "createAccount"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__createAccount__pfield__customerId__0
	"customerId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield
	qml_wsdl_import__createAccount__pfield__newAccountType__0 "newAccountType"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield
	qml_wsdl_import__createAccount__pfield__fromAccountId__0 "fromAccountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_createAccountResponse__0
"SOAP Envelope for createAccountResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__createAccountResponse_32_message__sfield__Header__0 "Header"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_createAccountResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__createAccountResponse_32_message__sfield__Body__0 "Body"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_createAccountResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_createAccountResponse__0
"SOAP Header for createAccountResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_createAccountResponse__0
"SOAP Body for createAccountResponse"
{
	creator.structuredfield
	qml_wsdl_import__createAccountResponse_32_body__sfield__createAccountResponse__0
	"createAccountResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__createAccountResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__createAccountResponse__0
"createAccountResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__createAccountResponse__sfield__account__0 "account"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__account__0;
}
creator.sequencetype qml_wsdl_import__seq__account__0 "account"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__account__pfield__id__0 "id"
		type = number;
	creator.primitivefield qml_wsdl_import__account__pfield__customerId__0
	"customerId"
		type = number;
	creator.enumerationfield qml_wsdl_import__account__enumfield__type__0 "type"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = qml_wsdl_import__enum__accountType__0
		optional;
	creator.primitivefield qml_wsdl_import__account__pfield__balance__0 "balance"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional;
}
creator.enum qml_wsdl_import__enum__accountType__0 "accountType"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.enumerationvalue qml_wsdl_import__accountType__enumval__CHECKING__0
	"CHECKING";
	creator.enumerationvalue qml_wsdl_import__accountType__enumval__SAVINGS__0
	"SAVINGS";
	creator.enumerationvalue qml_wsdl_import__accountType__enumval__LOAN__0
	"LOAN";
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_startupJmsListener__0
"SOAP Envelope for startupJmsListener"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__startupJmsListener_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_startupJmsListener__0
		optional;
	creator.structuredfield
	qml_wsdl_import__startupJmsListener_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_startupJmsListener__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_startupJmsListener__0
"SOAP Header for startupJmsListener"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_startupJmsListener__0
"SOAP Body for startupJmsListener"
{
	creator.structuredfield
	qml_wsdl_import__startupJmsListener_32_body__sfield__startupJmsListener__0
	"startupJmsListener"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__startupJmsListener__0;
}
creator.sequencetype qml_wsdl_import__seq__startupJmsListener__0
"startupJmsListener"
	namespace = "http://service.parabank.parasoft.com/"
{
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_startupJmsListenerResponse__0
"SOAP Envelope for startupJmsListenerResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__startupJmsListenerResponse_32_message__sfield__Header__0
	"Header"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_startupJmsListenerResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__startupJmsListenerResponse_32_message__sfield__Body__0
	"Body"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_startupJmsListenerResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_startupJmsListenerResponse__0
"SOAP Header for startupJmsListenerResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_startupJmsListenerResponse__0
"SOAP Body for startupJmsListenerResponse"
{
	creator.structuredfield
	qml_wsdl_import__startupJmsListenerResponse_32_body__sfield__startupJmsListenerResponse__0
	"startupJmsListenerResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__startupJmsListenerResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__startupJmsListenerResponse__0
"startupJmsListenerResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
}
creator.message qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_login__0
"SOAP Envelope for login"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield qml_wsdl_import__login_32_message__sfield__Header__0
	"Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_login__0
		optional;
	creator.structuredfield qml_wsdl_import__login_32_message__sfield__Body__0
	"Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_login__0;
}
creator.sequencetype qml_wsdl_import__seq__SOAP_32_Header_32_for_32_login__0
"SOAP Header for login"
{
}
creator.sequencetype qml_wsdl_import__seq__SOAP_32_Body_32_for_32_login__0
"SOAP Body for login"
{
	creator.structuredfield qml_wsdl_import__login_32_body__sfield__login__0
	"login"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__login__0;
}
creator.sequencetype qml_wsdl_import__seq__login__0 "login"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__login__pfield__username__0 "username"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__login__pfield__password__0 "password"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_loginResponse__0
"SOAP Envelope for loginResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__loginResponse_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_loginResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__loginResponse_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_loginResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_loginResponse__0
"SOAP Header for loginResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_loginResponse__0
"SOAP Body for loginResponse"
{
	creator.structuredfield
	qml_wsdl_import__loginResponse_32_body__sfield__loginResponse__0
	"loginResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__loginResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__loginResponse__0 "loginResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield qml_wsdl_import__loginResponse__sfield__customerId__0
	"customerId"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__customer__0;
}
creator.message qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_deposit__0
"SOAP Envelope for deposit"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__deposit_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_deposit__0
		optional;
	creator.structuredfield qml_wsdl_import__deposit_32_message__sfield__Body__0
	"Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_deposit__0;
}
creator.sequencetype qml_wsdl_import__seq__SOAP_32_Header_32_for_32_deposit__0
"SOAP Header for deposit"
{
}
creator.sequencetype qml_wsdl_import__seq__SOAP_32_Body_32_for_32_deposit__0
"SOAP Body for deposit"
{
	creator.structuredfield qml_wsdl_import__deposit_32_body__sfield__deposit__0
	"deposit"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__deposit__0;
}
creator.sequencetype qml_wsdl_import__seq__deposit__0 "deposit"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__deposit__pfield__accountId__0
	"accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__deposit__pfield__amount__0 "amount"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_depositResponse__0
"SOAP Envelope for depositResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__depositResponse_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_depositResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__depositResponse_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_depositResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_depositResponse__0
"SOAP Header for depositResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_depositResponse__0
"SOAP Body for depositResponse"
{
	creator.structuredfield
	qml_wsdl_import__depositResponse_32_body__sfield__depositResponse__0
	"depositResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__depositResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__depositResponse__0
"depositResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield
	qml_wsdl_import__depositResponse__pfield__depositReturn__0 "depositReturn"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactionsByAmount__0
"SOAP Envelope for getTransactionsByAmount"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByAmount_32_message__sfield__Header__0
	"Header"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByAmount__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactionsByAmount_32_message__sfield__Body__0 "Body"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByAmount__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByAmount__0
"SOAP Header for getTransactionsByAmount"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByAmount__0
"SOAP Body for getTransactionsByAmount"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByAmount_32_body__sfield__getTransactionsByAmount__0
	"getTransactionsByAmount"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactionsByAmount__0;
}
creator.sequencetype qml_wsdl_import__seq__getTransactionsByAmount__0
"getTransactionsByAmount"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield
	qml_wsdl_import__getTransactionsByAmount__pfield__accountId__0 "accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield
	qml_wsdl_import__getTransactionsByAmount__pfield__amount__0 "amount"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactionsByAmountResponse__0
"SOAP Envelope for getTransactionsByAmountResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByAmountResponse_32_message__sfield__Header__0
	"Header"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByAmountResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactionsByAmountResponse_32_message__sfield__Body__0
	"Body"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByAmountResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByAmountResponse__0
"SOAP Header for getTransactionsByAmountResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByAmountResponse__0
"SOAP Body for getTransactionsByAmountResponse"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByAmountResponse_32_body__sfield__getTransactionsByAmountResponse__0
	"getTransactionsByAmountResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactionsByAmountResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__getTransactionsByAmountResponse__0
"getTransactionsByAmountResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByAmountResponse__sfield__transaction__0
	"transaction"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__transaction__0 array;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_setParameter__0
"SOAP Envelope for setParameter"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__setParameter_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_setParameter__0
		optional;
	creator.structuredfield
	qml_wsdl_import__setParameter_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_setParameter__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_setParameter__0
"SOAP Header for setParameter"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_setParameter__0
"SOAP Body for setParameter"
{
	creator.structuredfield
	qml_wsdl_import__setParameter_32_body__sfield__setParameter__0 "setParameter"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__setParameter__0;
}
creator.sequencetype qml_wsdl_import__seq__setParameter__0 "setParameter"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__setParameter__pfield__name__0 "name"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__setParameter__pfield__value__0
	"value"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_setParameterResponse__0
"SOAP Envelope for setParameterResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__setParameterResponse_32_message__sfield__Header__0 "Header"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_setParameterResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__setParameterResponse_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_setParameterResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_setParameterResponse__0
"SOAP Header for setParameterResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_setParameterResponse__0
"SOAP Body for setParameterResponse"
{
	creator.structuredfield
	qml_wsdl_import__setParameterResponse_32_body__sfield__setParameterResponse__0
	"setParameterResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__setParameterResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__setParameterResponse__0
"setParameterResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getPositions__0
"SOAP Envelope for getPositions"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getPositions_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPositions__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getPositions_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPositions__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPositions__0
"SOAP Header for getPositions"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPositions__0
"SOAP Body for getPositions"
{
	creator.structuredfield
	qml_wsdl_import__getPositions_32_body__sfield__getPositions__0 "getPositions"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getPositions__0;
}
creator.sequencetype qml_wsdl_import__seq__getPositions__0 "getPositions"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__getPositions__pfield__customerId__0
	"customerId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getPositionsResponse__0
"SOAP Envelope for getPositionsResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getPositionsResponse_32_message__sfield__Header__0 "Header"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPositionsResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getPositionsResponse_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPositionsResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPositionsResponse__0
"SOAP Header for getPositionsResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPositionsResponse__0
"SOAP Body for getPositionsResponse"
{
	creator.structuredfield
	qml_wsdl_import__getPositionsResponse_32_body__sfield__getPositionsResponse__0
	"getPositionsResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getPositionsResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__getPositionsResponse__0
"getPositionsResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getPositionsResponse__sfield__position__0 "position"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__position__0 array;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactionsByMonthAndType__0
"SOAP Envelope for getTransactionsByMonthAndType"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByMonthAndType_32_message__sfield__Header__0
	"Header"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByMonthAndType__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactionsByMonthAndType_32_message__sfield__Body__0
	"Body"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByMonthAndType__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByMonthAndType__0
"SOAP Header for getTransactionsByMonthAndType"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByMonthAndType__0
"SOAP Body for getTransactionsByMonthAndType"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByMonthAndType_32_body__sfield__getTransactionsByMonthAndType__0
	"getTransactionsByMonthAndType"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactionsByMonthAndType__0;
}
creator.sequencetype qml_wsdl_import__seq__getTransactionsByMonthAndType__0
"getTransactionsByMonthAndType"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield
	qml_wsdl_import__getTransactionsByMonthAndType__pfield__accountId__0
	"accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield
	qml_wsdl_import__getTransactionsByMonthAndType__pfield__month__0 "month"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield
	qml_wsdl_import__getTransactionsByMonthAndType__pfield__type__0 "type"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactionsByMonthAndTypeResponse__0
"SOAP Envelope for getTransactionsByMonthAndTypeResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByMonthAndTypeResponse_32_message__sfield__Header__0
	"Header"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByMonthAndTypeResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactionsByMonthAndTypeResponse_32_message__sfield__Body__0
	"Body"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByMonthAndTypeResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByMonthAndTypeResponse__0
"SOAP Header for getTransactionsByMonthAndTypeResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByMonthAndTypeResponse__0
"SOAP Body for getTransactionsByMonthAndTypeResponse"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByMonthAndTypeResponse_32_body__sfield__getTransactionsByMonthAndTypeResponse__0
	"getTransactionsByMonthAndTypeResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactionsByMonthAndTypeResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__getTransactionsByMonthAndTypeResponse__0
"getTransactionsByMonthAndTypeResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByMonthAndTypeResponse__sfield__transaction__0
	"transaction"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__transaction__0 array;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactions__0
"SOAP Envelope for getTransactions"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactions_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactions__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactions_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactions__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactions__0
"SOAP Header for getTransactions"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactions__0
"SOAP Body for getTransactions"
{
	creator.structuredfield
	qml_wsdl_import__getTransactions_32_body__sfield__getTransactions__0
	"getTransactions"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactions__0;
}
creator.sequencetype qml_wsdl_import__seq__getTransactions__0
"getTransactions"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__getTransactions__pfield__accountId__0
	"accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactionsResponse__0
"SOAP Envelope for getTransactionsResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsResponse_32_message__sfield__Header__0
	"Header"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactionsResponse_32_message__sfield__Body__0 "Body"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsResponse__0
"SOAP Header for getTransactionsResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsResponse__0
"SOAP Body for getTransactionsResponse"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsResponse_32_body__sfield__getTransactionsResponse__0
	"getTransactionsResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactionsResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__getTransactionsResponse__0
"getTransactionsResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsResponse__sfield__transaction__0
	"transaction"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__transaction__0 array;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactionsByToFromDate__0
"SOAP Envelope for getTransactionsByToFromDate"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByToFromDate_32_message__sfield__Header__0
	"Header"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByToFromDate__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactionsByToFromDate_32_message__sfield__Body__0
	"Body"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByToFromDate__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByToFromDate__0
"SOAP Header for getTransactionsByToFromDate"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByToFromDate__0
"SOAP Body for getTransactionsByToFromDate"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByToFromDate_32_body__sfield__getTransactionsByToFromDate__0
	"getTransactionsByToFromDate"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactionsByToFromDate__0;
}
creator.sequencetype qml_wsdl_import__seq__getTransactionsByToFromDate__0
"getTransactionsByToFromDate"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield
	qml_wsdl_import__getTransactionsByToFromDate__pfield__accountId__0
	"accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield
	qml_wsdl_import__getTransactionsByToFromDate__pfield__fromDate__0 "fromDate"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield
	qml_wsdl_import__getTransactionsByToFromDate__pfield__toDate__0 "toDate"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactionsByToFromDateResponse__0
"SOAP Envelope for getTransactionsByToFromDateResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByToFromDateResponse_32_message__sfield__Header__0
	"Header"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByToFromDateResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactionsByToFromDateResponse_32_message__sfield__Body__0
	"Body"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByToFromDateResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByToFromDateResponse__0
"SOAP Header for getTransactionsByToFromDateResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByToFromDateResponse__0
"SOAP Body for getTransactionsByToFromDateResponse"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByToFromDateResponse_32_body__sfield__getTransactionsByToFromDateResponse__0
	"getTransactionsByToFromDateResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactionsByToFromDateResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__getTransactionsByToFromDateResponse__0
"getTransactionsByToFromDateResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByToFromDateResponse__sfield__transaction__0
	"transaction"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__transaction__0 array;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_requestLoan__0
"SOAP Envelope for requestLoan"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__requestLoan_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_requestLoan__0
		optional;
	creator.structuredfield
	qml_wsdl_import__requestLoan_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_requestLoan__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_requestLoan__0
"SOAP Header for requestLoan"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_requestLoan__0
"SOAP Body for requestLoan"
{
	creator.structuredfield
	qml_wsdl_import__requestLoan_32_body__sfield__requestLoan__0 "requestLoan"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__requestLoan__0;
}
creator.sequencetype qml_wsdl_import__seq__requestLoan__0 "requestLoan"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__requestLoan__pfield__customerId__0
	"customerId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__requestLoan__pfield__amount__0
	"amount"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__requestLoan__pfield__downPayment__0
	"downPayment"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__requestLoan__pfield__fromAccountId__0
	"fromAccountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_requestLoanResponse__0
"SOAP Envelope for requestLoanResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__requestLoanResponse_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_requestLoanResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__requestLoanResponse_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_requestLoanResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_requestLoanResponse__0
"SOAP Header for requestLoanResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_requestLoanResponse__0
"SOAP Body for requestLoanResponse"
{
	creator.structuredfield
	qml_wsdl_import__requestLoanResponse_32_body__sfield__requestLoanResponse__0
	"requestLoanResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__requestLoanResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__requestLoanResponse__0
"requestLoanResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__requestLoanResponse__sfield__loanResponse__0 "loanResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__loanResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__loanResponse__0 "loanResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__loanResponse__pfield__responseDate__0
	"responseDate"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional;
	creator.primitivefield
	qml_wsdl_import__loanResponse__pfield__loanProviderName__0 "loanProviderName"
		type = String;
	creator.primitivefield qml_wsdl_import__loanResponse__pfield__approved__0
	"approved"
		type = boolean;
	creator.primitivefield qml_wsdl_import__loanResponse__pfield__message__0
	"message"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__loanResponse__pfield__accountId__0
	"accountId"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = number
		optional;
}
creator.message qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_withdraw__0
"SOAP Envelope for withdraw"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__withdraw_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_withdraw__0
		optional;
	creator.structuredfield qml_wsdl_import__withdraw_32_message__sfield__Body__0
	"Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_withdraw__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_withdraw__0
"SOAP Header for withdraw"
{
}
creator.sequencetype qml_wsdl_import__seq__SOAP_32_Body_32_for_32_withdraw__0
"SOAP Body for withdraw"
{
	creator.structuredfield
	qml_wsdl_import__withdraw_32_body__sfield__withdraw__0 "withdraw"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__withdraw__0;
}
creator.sequencetype qml_wsdl_import__seq__withdraw__0 "withdraw"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__withdraw__pfield__accountId__0
	"accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__withdraw__pfield__amount__0 "amount"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_withdrawResponse__0
"SOAP Envelope for withdrawResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__withdrawResponse_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_withdrawResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__withdrawResponse_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_withdrawResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_withdrawResponse__0
"SOAP Header for withdrawResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_withdrawResponse__0
"SOAP Body for withdrawResponse"
{
	creator.structuredfield
	qml_wsdl_import__withdrawResponse_32_body__sfield__withdrawResponse__0
	"withdrawResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__withdrawResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__withdrawResponse__0
"withdrawResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield
	qml_wsdl_import__withdrawResponse__pfield__withdrawReturn__0 "withdrawReturn"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getAccounts__0
"SOAP Envelope for getAccounts"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getAccounts_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getAccounts__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getAccounts_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getAccounts__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getAccounts__0
"SOAP Header for getAccounts"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getAccounts__0
"SOAP Body for getAccounts"
{
	creator.structuredfield
	qml_wsdl_import__getAccounts_32_body__sfield__getAccounts__0 "getAccounts"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getAccounts__0;
}
creator.sequencetype qml_wsdl_import__seq__getAccounts__0 "getAccounts"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__getAccounts__pfield__customerId__0
	"customerId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getAccountsResponse__0
"SOAP Envelope for getAccountsResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getAccountsResponse_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getAccountsResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getAccountsResponse_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getAccountsResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getAccountsResponse__0
"SOAP Header for getAccountsResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getAccountsResponse__0
"SOAP Body for getAccountsResponse"
{
	creator.structuredfield
	qml_wsdl_import__getAccountsResponse_32_body__sfield__getAccountsResponse__0
	"getAccountsResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getAccountsResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__getAccountsResponse__0
"getAccountsResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getAccountsResponse__sfield__account__0 "account"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__account__0 array;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_buyPosition__0
"SOAP Envelope for buyPosition"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__buyPosition_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_buyPosition__0
		optional;
	creator.structuredfield
	qml_wsdl_import__buyPosition_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_buyPosition__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_buyPosition__0
"SOAP Header for buyPosition"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_buyPosition__0
"SOAP Body for buyPosition"
{
	creator.structuredfield
	qml_wsdl_import__buyPosition_32_body__sfield__buyPosition__0 "buyPosition"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__buyPosition__0;
}
creator.sequencetype qml_wsdl_import__seq__buyPosition__0 "buyPosition"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__buyPosition__pfield__customerId__0
	"customerId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__buyPosition__pfield__accountId__0
	"accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__buyPosition__pfield__name__0 "name"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__buyPosition__pfield__symbol__0
	"symbol"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__buyPosition__pfield__shares__0
	"shares"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__buyPosition__pfield__pricePerShare__0
	"pricePerShare"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_buyPositionResponse__0
"SOAP Envelope for buyPositionResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__buyPositionResponse_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_buyPositionResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__buyPositionResponse_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_buyPositionResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_buyPositionResponse__0
"SOAP Header for buyPositionResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_buyPositionResponse__0
"SOAP Body for buyPositionResponse"
{
	creator.structuredfield
	qml_wsdl_import__buyPositionResponse_32_body__sfield__buyPositionResponse__0
	"buyPositionResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__buyPositionResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__buyPositionResponse__0
"buyPositionResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__buyPositionResponse__sfield__position__0 "position"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__position__0 array;
}
creator.message qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_cleanDB__0
"SOAP Envelope for cleanDB"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__cleanDB_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_cleanDB__0
		optional;
	creator.structuredfield qml_wsdl_import__cleanDB_32_message__sfield__Body__0
	"Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_cleanDB__0;
}
creator.sequencetype qml_wsdl_import__seq__SOAP_32_Header_32_for_32_cleanDB__0
"SOAP Header for cleanDB"
{
}
creator.sequencetype qml_wsdl_import__seq__SOAP_32_Body_32_for_32_cleanDB__0
"SOAP Body for cleanDB"
{
	creator.structuredfield qml_wsdl_import__cleanDB_32_body__sfield__cleanDB__0
	"cleanDB"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__cleanDB__0;
}
creator.sequencetype qml_wsdl_import__seq__cleanDB__0 "cleanDB"
	namespace = "http://service.parabank.parasoft.com/"
{
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_cleanDBResponse__0
"SOAP Envelope for cleanDBResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__cleanDBResponse_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_cleanDBResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__cleanDBResponse_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_cleanDBResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_cleanDBResponse__0
"SOAP Header for cleanDBResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_cleanDBResponse__0
"SOAP Body for cleanDBResponse"
{
	creator.structuredfield
	qml_wsdl_import__cleanDBResponse_32_body__sfield__cleanDBResponse__0
	"cleanDBResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__cleanDBResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__cleanDBResponse__0
"cleanDBResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
}
creator.message qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getAccount__0
"SOAP Envelope for getAccount"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getAccount_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getAccount__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getAccount_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getAccount__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getAccount__0
"SOAP Header for getAccount"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getAccount__0
"SOAP Body for getAccount"
{
	creator.structuredfield
	qml_wsdl_import__getAccount_32_body__sfield__getAccount__0 "getAccount"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getAccount__0;
}
creator.sequencetype qml_wsdl_import__seq__getAccount__0 "getAccount"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__getAccount__pfield__accountId__0
	"accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getAccountResponse__0
"SOAP Envelope for getAccountResponse"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getAccountResponse_32_message__sfield__Header__0 "Header"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getAccountResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getAccountResponse_32_message__sfield__Body__0 "Body"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getAccountResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getAccountResponse__0
"SOAP Header for getAccountResponse"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getAccountResponse__0
"SOAP Body for getAccountResponse"
{
	creator.structuredfield
	qml_wsdl_import__getAccountResponse_32_body__sfield__getAccountResponse__0
	"getAccountResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getAccountResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__getAccountResponse__0
"getAccountResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getAccountResponse__sfield__account__0 "account"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__account__0;
}
creator.gui.screen qml657ba737ad4e44aabbc66604b971eb73 "Login"
{
	creator.gui.button qml9bfdc7ab87c34e37876f9e98c00c56eb "Login"
		status = dontcare;
	creator.gui.form qml580a6f761cb44261bec8b62c619eb594 "Customer Login"
	{
		creator.gui.textbox qml2424df548a1c447196674567bbf9be1f "Username"
			type = String
			status = dontcare;
		creator.gui.textbox qml2cc886170e6a4ae89fa02e01d0c578ff "Password"
			type = String
			status = dontcare;
	}
	creator.gui.labelwidget qmle5ac3a5579ae4ad484891c3a056d4b63 "Status"
		status = dontcare;
	creator.gui.form qml992fe16f170040a0b96aa6cdfc130986 "unnamed"
		deleted
	{
	}
}
creator.gui.screen qml799f0eead238440298b9eef8caf6a7fb "Home"
{
	creator.gui.hyperlink qml91d44c31d5de48ae97719b14c134b080 "Open New Account"
		status = dontcare;
	creator.gui.hyperlink qmlddc8eba7c0f6434d9e2c1bc49b23c333 "Transfer Funds"
		status = dontcare;
	creator.gui.hyperlink qmlb04f4fc9360249a9982209b589d8ef31 "Logout"
		status = dontcare;
	creator.gui.labelwidget qml5e153a7fb022452db1c20b50c3ef3971 "Status"
		status = dontcare;
}
creator.gui.screen qml5d6d91143789435c93b33a8670cc1df2 "Open New Account"
{
	creator.gui.form qmld8cb98287bba4b7a83152c96eccde64f "Open New Account"
	{
		creator.gui.dropdown qmleee1626abaaf42d293ea7cf93f39fd67 "Account Type"
			type = qmlff496ed971144f4489f7122a23a73edf
			status = dontcare;
		creator.gui.dropdown qml8b674cfc6e02432fafc13430a1904b28 "Account Number"
			type = qml6823d4016dd0421995442e32843216b4
			status = dontcare;
	}
	creator.gui.button qmlca7c45bc2bfd48cf80d59570b4a88c95 "Open New Account"
		status = dontcare;
	creator.gui.labelwidget qml6827ebd8e68940799cb5d963ca5b145b "Status"
		status = dontcare;
}
creator.enum qmlff496ed971144f4489f7122a23a73edf "Account Type"
{
	creator.enumerationvalue qmlb463d9a902484637b82f479367522e61 "Checking";
	creator.enumerationvalue qmlbe0248da0a2e4ad4b46d37d7dbf8b0f7 "Savings";
}
creator.enum qml6823d4016dd0421995442e32843216b4 "Account Number"
{
	creator.enumerationvalue qml66062adcfeee47fa904c8beaa7b1d091 "12444";
	creator.enumerationvalue qml1bba20421bbe4dfebbb199668d7b96d2 "23546";
}
creator.gui.screen qml64dfa39e7f2d4573947741237cdd064f "Transfer Funds"
{
	creator.gui.form qmla819126dbe6d4dd685255f03bd000f99 "Transfer Funds"
	{
		creator.gui.textbox qmlfe9c63764c58464c89fcb9867915f4e0 "Amount"
			type = String
			status = dontcare;
		creator.gui.dropdown qml0a20fc5fa0234a30b1cbd040d4a73cff "From Account"
			type = qml6823d4016dd0421995442e32843216b4
			status = dontcare;
		creator.gui.dropdown qml25a546fbf6f346e888502ef497e1bdb6 "To Account"
			type = qml6823d4016dd0421995442e32843216b4
			status = dontcare;
	}
	creator.gui.button qmlcedd167d22314c648128ab0b258642e0 "Transfer"
		status = dontcare;
}
creator.message qml3daa70399ad04b019c5b88d1998f2df0 "User Info Request"
	interfaces = [ qml164066e681c7470d95f29696ac4c53a1 ]
{
	creator.primitivefield qml9e4c1d53a32d4120a5979ecd0ead6a5f "Name"
		type = String;
	creator.enumerationfield qml386465615b6f4717ae58d6029fa45773 "Gender"
		type = qml97d18c28631e4c139b338276e5fcfbab;
	creator.structuredfield qml81d6a3bb833c4d8d80169b9302732057 "Address"
		type = qml7c1ae16de5e749c2a2dc9e6078d863a4;
}
creator.externalinterface qml164066e681c7470d95f29696ac4c53a1 "User1"
	direction = in;
creator.enum qml97d18c28631e4c139b338276e5fcfbab "Gender1"
{
	creator.enumerationvalue qml296afd21e7334bbd9e290aceb34aa0b5 "M";
	creator.enumerationvalue qmlb0ae9fef27404c64911133f910da4bf1 "F";
}
creator.sequencetype qml430dad7fd37d4f149e71735d78d2b833 "Address"
{
	creator.primitivefield qmlb24863e915394f729c60b169422b0402 "S.No"
		type = String;
	creator.primitivefield qml4a00aef1d58049e6bab4191a10abb89c "City"
		type = String;
	creator.primitivefield qmld9ec6f8bf0bf499aaf8def17df7be0d8 "Country"
		type = String;
	creator.primitivefield qml73f7e1d03aa345809168a29f2dc98265 "ZipCode"
		type = String;
}
creator.sequencetype qml7c1ae16de5e749c2a2dc9e6078d863a4 "Address1"
{
	creator.structuredfield qmle33ed11e717140ca9d13824be39f8e46
	"Permanent Address"
		type = qml430dad7fd37d4f149e71735d78d2b833;
	creator.primitivefield qml332db663da294620933ac768514f0776 "unnamed"
		type = String
		deleted;
	creator.structuredfield qmlb799e23f1e364024883b7289e81b0cba "Temp Address"
		type = qml430dad7fd37d4f149e71735d78d2b833;
}
creator.datatable qml77c271469f664438a99a6076281088aa "unnamed"
	deleted
{
	creator.primitivecell qmlbb53e3056de54d7aa0d03e5c840065e2 "unnamed"
		type = String
		deleted;
}
creator.gui.screen qml3eb9a2432db046608ba9428eaaede78c "Make My Trip"
{
	creator.gui.uitable qml03a0b020fd5c4061b31e78a70cefeb0c "Flight details"
	datatable = qml3db34221f3074eb78280ab1360f9bd6c
	{
		creator.gui.button qmlefd36f682a784c20847d529f8bd9c734 "Select"
			status = dontcare;
		creator.gui.hyperlink qml26531a4688684641951140cff8d3b4d0 "Flight Details"
			status = dontcare;
	}
}
creator.datatable qml3db34221f3074eb78280ab1360f9bd6c "Flight Info"
{
	creator.primitivecell qml66c494e2fe6e4a57991b45538e058cef "Flight Name"
		type = String;
	creator.primitivecell qmld93e8fc1fb114a04bbea67c2849503eb "Src"
		type = String;
	creator.primitivecell qml564ea550cf4048c19a0ade31f4b6f263 "Destination"
		type = String;
	creator.primitivecell qmlfd1482e469aa4499b6a0466510f973e8 "From Time"
		type = String;
	creator.primitivecell qml1c70bacd81f045fb822bd54fb6955fed "To Time"
		type = String;
	creator.primitivecell qmle44c713647344eb981a8ff6a3bd6e880 "Price"
		type = String;
}
creator.gui.screen qmlcd623c389e6848dc9c1f9de3a8f3a278 "Order deatils"
{
	creator.gui.form qml4740f2bfb605430593dda60b252aef13 "Ord Info"
	{
		creator.gui.textbox qml2784dbba8fc14832ad3276f6ece996d0 "OrdName"
			type = String
			status = dontcare;
		creator.gui.textbox qmlfd255a589cf24c2f97c752c4dad3a57f "Qty"
			type = String
			status = dontcare;
	}
}
creator.gui.screen qml44d51e185da346af88c0005e54f51b7e "Screen"
{
	creator.gui.button qml3d1cb9e31f854187bf2acab80ea10070 "Btn"
		status = dontcare;
	creator.gui.hyperlink qml6b89a2b590954ff29b009317b1e6ebca "Link"
		status = dontcare;
	creator.gui.form qml053638dbf2e44e64968d54c5ffcd437c "Form"
	{
		creator.gui.button qmlffc7bb02548d4c9199d56ce81f935c02 "Btn"
			status = dontcare;
		creator.gui.hyperlink qmlbc9891bf21bf4456a96f81e66d71fe08 "Link"
			status = dontcare;
		creator.gui.labelwidget qmlb6862c6c61ee4de78710dcb9ee9624be "Label"
			status = dontcare;
		creator.gui.textbox qml959cd8bfbfeb497d80f013b894f6c842 "TB"
			type = String
			status = dontcare;
		creator.gui.checkbox qml892eef10987749a3b53fbad72ef190a4 "CB"
			status = dontcare
			checked = dontcare;
		creator.gui.dropdown qml76685c693fd34c318c7acc384d6414d6 "DD"
			type = qml73520b864a1c4ded8a6c4d7e1b26639e
			status = dontcare;
		creator.gui.radiobutton qml76baefe52754450898cb753314fc5a4e "RB"
			type = qml73520b864a1c4ded8a6c4d7e1b26639e
			status = dontcare;
		creator.gui.calendar qml3d586ae73d0d400faa55123e96e48c5f "Calendar"
			status = dontcare;
		creator.gui.listbox qmlbea53e2cec6042abb0ce1820150d9a51 "LB"
			status = dontcare
			items = [ ];
	}
	creator.gui.labelwidget qml253e46c2b14c48bea06fe56cea10e48a "Label"
		status = dontcare;
	creator.gui.menubar qml46e5617f37cc46c89088a3f79a69bb41 "Menus"
	{
		creator.gui.menu qml61961f93b9d2482c9dc23aee9cb4c385 "File"
		{
			creator.gui.clickchoice qml2b1824e879ac435eb90d2233bb6c5c39 "Click";
			creator.gui.mouseoverchoice qmlc1cad13f2bf14fa1a0f86612ca89eccb
			"Mouse over";
			creator.gui.menu qml03a5e84b737b4813ab28d7853e3314ea "SubMenus"
			{
			}
		}
	}
	creator.gui.tab qmlacb490530dae4afa8ffe7158fd652e95 "Modeling"
		selected
	{
	}
	creator.gui.tab qml496a946617ab476f9d2a2b807fc55d9e "Test Design"
	{
	}
	creator.gui.group qmla1b3c9e7a2144cc7a4b91de566b156b3 "unnamed"
	{
	}
	creator.gui.treenode qml5653e58820f24779ada94d76fa88cb73 "Project explorer"
	{
		creator.gui.treenode qmlfea1a454d3ed4a5298e0db5b1aa7bf3f "Day1"
		{
			creator.gui.treenode qmle0adc4f601704f48a3e6e09e11a64d4f "DC"
			{
				creator.gui.treenode qmlc14da9ee91044017b0155f37fa95318d "ExcelScripter"
				{
				}
			}
			creator.gui.treenode qmle5506b125d114036b75600b4c0692f7d
			"ConformiQ Options"
			{
			}
			creator.gui.treenode qmldb3751473d46416e8898906e86bf7821
			"Requirement Providers"
			{
			}
			creator.gui.treenode qmlcca7b38eb22a4bdc8519632a25358d2a "Model"
			{
				creator.gui.treenode qmle600dbf506f44b58b38da4eb5f4f8726 "AD"
				{
				}
				creator.gui.treenode qml162973f653fe416d8082bfa981de569d "SD"
				{
				}
				creator.gui.treenode qml7893202ad1aa4716b8351377d49d2b68 ".sl"
				{
				}
			}
		}
	}
}
creator.enum qml73520b864a1c4ded8a6c4d7e1b26639e "Gender"
{
	creator.enumerationvalue qml680ae28d1cb54eb2b6c4f0529610ba30 "F";
	creator.enumerationvalue qml0de1fff829a441019ea247b7d5d5490f "M";
}
creator.gui.popup qmlf9addb71d5b24eb399ace37cc7b7c5c9 "Popup"
{
}
creator.customaction qml800fc4bac6e04466b1c87d92730cad52 "RightClick"
	interfaces = [ qml3554dfe29cd54a3a94e110c0c62a21f3 ]
	shortform = "RC"
	direction = in
	tokens = [ literal "Click on " reference qml646835af2be8449985600885f3cb6f5d
literal "Button" ]
{
	creator.primitivefield qml646835af2be8449985600885f3cb6f5d "Button Name"
		type = String;
}
creator.externalinterface qml3554dfe29cd54a3a94e110c0c62a21f3 "User123"
	direction = in;
creator.customaction qml622100c087f74815a69c436da7df5b25 "Verify Status"
	interfaces = [ qmlc7d2fd098d75418cbbfe02c8a91acc4b ]
	shortform = "VS"
	direction = out
	tokens = [ literal "Verify status as : " reference
qmlbe6c14e0b3b5421aaebed8054d99dbf6 ]
{
	creator.primitivefield qmlbe6c14e0b3b5421aaebed8054d99dbf6 "Status"
		type = String;
}
creator.externalinterface qmlc7d2fd098d75418cbbfe02c8a91acc4b "Display123"
	direction = out;
creator.customaction qml_8a18ae80b4e34f1484bc6b2bdec56e35 "Allocate car"
	interfaces = [ qml_4edf2ecf876c4469b9a6becbed7e15ed ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Allocate car" ]
{
}
creator.customaction qml_396fe00d3f53475b917da841450e5460
"_verfication_Car is allocated"
	interfaces = [ qml_e56a1906d2d34574968d443d94bcdfbf ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Car is allocated has completed " ]
{
}
creator.customaction qml_1a0e087d931e46de92f07929d9a7c6a7
"_verfication_Request Rejected"
	interfaces = [ qml_e56a1906d2d34574968d443d94bcdfbf ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Request Rejected has completed " ]
{
}
creator.externalinterface qml_4edf2ecf876c4469b9a6becbed7e15ed "User"
	direction = in;
creator.customaction qml_2298c50bff6744648b1ed35609389011
"_verfication_Allocate car"
	interfaces = [ qml_e56a1906d2d34574968d443d94bcdfbf ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Allocate car has completed " ]
{
}
creator.customaction qml_ab564ddd318a4ebf81959c797b85b333
"_verfication_Review Request"
	interfaces = [ qml_e56a1906d2d34574968d443d94bcdfbf ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Review Request has completed " ]
{
}
creator.customaction qml_d36d5e5c816940fd98a746937a271583
"_verfication_Platform Transportation"
	interfaces = [ qml_e56a1906d2d34574968d443d94bcdfbf ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Platform Transportation has completed " ]
{
}
creator.customaction qml_b040576e5c94442b87d135e0e9b7b659
"Platform Transportation"
	interfaces = [ qml_4edf2ecf876c4469b9a6becbed7e15ed ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Platform Transportation" ]
{
}
creator.customaction qml_67c40f99b7a74cb9832830b5f521a8b8
"_verfication_Register request"
	interfaces = [ qml_e56a1906d2d34574968d443d94bcdfbf ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Register request has completed " ]
{
}
creator.customaction qml_8d3c97bf906b4ce5a8ab8596741f3c26 "Register request"
	interfaces = [ qml_4edf2ecf876c4469b9a6becbed7e15ed ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Register request" ]
{
}
creator.customaction qml_86312b9824fd4e4db0a11b833d433429 "Car is allocated"
	interfaces = [ qml_4edf2ecf876c4469b9a6becbed7e15ed ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Car is allocated" ]
{
}
creator.customaction qml_bc308ffe73af4ecbb0acf967443dfc22 "Is Option?"
	interfaces = [ qml_4edf2ecf876c4469b9a6becbed7e15ed ]
	shortform = "AI"
	direction = in
	tokens = [ literal "Option is " ]
{
}
creator.customaction qml_1abddc7c08e446e8b5c6c7b9012b2988 "Review Request"
	interfaces = [ qml_4edf2ecf876c4469b9a6becbed7e15ed ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Review Request" ]
{
}
creator.externalinterface qml_e56a1906d2d34574968d443d94bcdfbf "System"
	direction = out;
creator.customaction qml_fac856d3070c46d1b3f981e261950c40 "Request Rejected"
	interfaces = [ qml_4edf2ecf876c4469b9a6becbed7e15ed ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Request Rejected" ]
{
}
creator.message
qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_onDate_2f__7b_onDate_7d__20_Response__message
"GET /accounts/{accountId}/transactions/onDate/{onDate} Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_onDate_2f__7b_onDate_7d__20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_onDate_2f__7b_onDate_7d__20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_onDate_2f__7b_onDate_7d__20_Response_20_payloads__choicetype
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_amount_2f__7b_amount_7d__20_Request__message
"GET /accounts/{accountId}/transactions/amount/{amount} Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_amount_2f__7b_amount_7d__20_Request__primitivefield__accountId
	"accountId"
		type = number
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_amount_2f__7b_amount_7d__20_Request__primitivefield__amount
	"amount"
		type = String
		optional
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__2f_accounts_20_Request__message
"GET /customers/{customerId}/accounts Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__2f_accounts_20_Request__primitivefield__customerId
	"customerId"
		type = number
		optional
		deleted;
}
creator.choicetype
qml_wadl_import__POST_20__2f_transfer_20_Response_20_payloads__choicetype
"POST /transfer Response payloads"
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_transfer_20_Response_20_payloads__primitivefield__application_2f_xml
	"application/xml"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_transfer_20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}
creator.choicetype
qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_onDate_2f__7b_onDate_7d__20_Response_20_payloads__choicetype
"GET /accounts/{accountId}/transactions/onDate/{onDate} Response payloads"
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_onDate_2f__7b_onDate_7d__20_Response_20_payloads__primitivefield__application_2f_xml
	"application/xml"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_onDate_2f__7b_onDate_7d__20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}
creator.choicetype
qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_amount_2f__7b_amount_7d__20_Response_20_payloads__choicetype
"GET /accounts/{accountId}/transactions/amount/{amount} Response payloads"
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_amount_2f__7b_amount_7d__20_Response_20_payloads__primitivefield__application_2f_xml
	"application/xml"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_amount_2f__7b_amount_7d__20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}
creator.message
qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_sellPosition_3f_accountId_3d__7b_accountId_7d__26_positionId_3d__7b_positionId_7d__26_shares_3d__7b_shares_7d__26_pricePerShare_3d__7b_pricePerShare_7d__20_Request__message
"POST /customers/{customerId}/sellPosition?accountId={accountId}&positionId={positionId}&shares={shares}&pricePerShare={pricePerShare} Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_sellPosition_3f_accountId_3d__7b_accountId_7d__26_positionId_3d__7b_positionId_7d__26_shares_3d__7b_shares_7d__26_pricePerShare_3d__7b_pricePerShare_7d__20_Request__primitivefield__customerId
	"customerId"
		type = number
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_sellPosition_3f_accountId_3d__7b_accountId_7d__26_positionId_3d__7b_positionId_7d__26_shares_3d__7b_shares_7d__26_pricePerShare_3d__7b_pricePerShare_7d__20_Request__primitivefield__accountId
	"accountId"
		type = number
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_sellPosition_3f_accountId_3d__7b_accountId_7d__26_positionId_3d__7b_positionId_7d__26_shares_3d__7b_shares_7d__26_pricePerShare_3d__7b_pricePerShare_7d__20_Request__primitivefield__positionId
	"positionId"
		type = number
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_sellPosition_3f_accountId_3d__7b_accountId_7d__26_positionId_3d__7b_positionId_7d__26_shares_3d__7b_shares_7d__26_pricePerShare_3d__7b_pricePerShare_7d__20_Request__primitivefield__shares
	"shares"
		type = number
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_sellPosition_3f_accountId_3d__7b_accountId_7d__26_positionId_3d__7b_positionId_7d__26_shares_3d__7b_shares_7d__26_pricePerShare_3d__7b_pricePerShare_7d__20_Request__primitivefield__pricePerShare
	"pricePerShare"
		type = String
		optional
		deleted;
}
creator.message qml_wadl_import__POST_20__2f_billpay_20_Response__message
"POST /billpay Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_billpay_20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__POST_20__2f_billpay_20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__POST_20__2f_billpay_20_Response_20_payloads__choicetype
		deleted;
}
creator.choicetype
qml_wadl_import__POST_20__2f_createAccount_20_Response_20_payloads__choicetype
"POST /createAccount Response payloads"
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_createAccount_20_Response_20_payloads__primitivefield__application_2f_xml
	"application/xml"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_createAccount_20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__20_Request__message
"GET /customers/{customerId} Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__20_Request__primitivefield__customerId
	"customerId"
		type = number
		optional
		deleted;
}
creator.message
qml_wadl_import__POST_20__2f_startupJmsListener_20_Response_20__5b_204_5d___message
"POST /startupJmsListener Response [204]"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_startupJmsListener_20_Response_20__5b_204_5d___primitivefield__status
	"status"
		type = number
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__20_Request__message
"GET /accounts/{accountId} Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__20_Request__primitivefield__accountId
	"accountId"
		type = number
		optional
		deleted;
}
creator.message
qml_wadl_import__POST_20__2f_cleanDB_20_Response_20__5b_204_5d___message
"POST /cleanDB Response [204]"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_cleanDB_20_Response_20__5b_204_5d___primitivefield__status
	"status"
		type = number
		deleted;
}
creator.message qml_wadl_import__POST_20__2f_transfer_20_Response__message
"POST /transfer Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_transfer_20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__POST_20__2f_transfer_20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__POST_20__2f_transfer_20_Response_20_payloads__choicetype
		deleted;
}
creator.message
qml_wadl_import__POST_20__2f_withdraw_3f_accountId_3d__7b_accountId_7d__26_amount_3d__7b_amount_7d__20_Request__message
"POST /withdraw?accountId={accountId}&amount={amount} Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_withdraw_3f_accountId_3d__7b_accountId_7d__26_amount_3d__7b_amount_7d__20_Request__primitivefield__accountId
	"accountId"
		type = number
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_withdraw_3f_accountId_3d__7b_accountId_7d__26_amount_3d__7b_amount_7d__20_Request__primitivefield__amount
	"amount"
		type = String
		optional
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_fromDate_2f__7b_fromDate_7d__2f_toDate_2f__7b_toDate_7d__20_Request__message
"GET /accounts/{accountId}/transactions/fromDate/{fromDate}/toDate/{toDate} Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_fromDate_2f__7b_fromDate_7d__2f_toDate_2f__7b_toDate_7d__20_Request__primitivefield__accountId
	"accountId"
		type = number
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_fromDate_2f__7b_fromDate_7d__2f_toDate_2f__7b_toDate_7d__20_Request__primitivefield__fromDate
	"fromDate"
		type = String
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_fromDate_2f__7b_fromDate_7d__2f_toDate_2f__7b_toDate_7d__20_Request__primitivefield__toDate
	"toDate"
		type = String
		optional
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_amount_2f__7b_amount_7d__20_Response__message
"GET /accounts/{accountId}/transactions/amount/{amount} Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_amount_2f__7b_amount_7d__20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_amount_2f__7b_amount_7d__20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_amount_2f__7b_amount_7d__20_Response_20_payloads__choicetype
		deleted;
}
creator.choicetype
qml_wadl_import__POST_20__2f_billpay_3f_accountId_3d__7b_accountId_7d__26_amount_3d__7b_amount_7d__20_Request_20_payloads__choicetype
"POST /billpay?accountId={accountId}&amount={amount} Request payloads"
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_billpay_3f_accountId_3d__7b_accountId_7d__26_amount_3d__7b_amount_7d__20_Request_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_billpay_3f_accountId_3d__7b_accountId_7d__26_amount_3d__7b_amount_7d__20_Request_20_payloads__primitivefield__application_2f_xml
	"application/xml"
		type = String
		deleted;
}
creator.message
qml_wadl_import__POST_20__2f_initializeDB_20_Response_20__5b_204_5d___message
"POST /initializeDB Response [204]"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_initializeDB_20_Response_20__5b_204_5d___primitivefield__status
	"status"
		type = number
		deleted;
}
creator.message
qml_wadl_import__POST_20__2f_createAccount_20_Response__message
"POST /createAccount Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_createAccount_20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__POST_20__2f_createAccount_20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__POST_20__2f_createAccount_20_Response_20_payloads__choicetype
		deleted;
}
creator.message
qml_wadl_import__POST_20__2f_setParameter_2f__7b_name_7d__2f__7b_value_7d__20_Request__message
"POST /setParameter/{name}/{value} Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_setParameter_2f__7b_name_7d__2f__7b_value_7d__20_Request__primitivefield__name
	"name"
		type = String
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_setParameter_2f__7b_name_7d__2f__7b_value_7d__20_Request__primitivefield__value
	"value"
		type = String
		optional
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_login_2f__7b_username_7d__2f__7b_password_7d__20_Response__message
"GET /login/{username}/{password} Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_login_2f__7b_username_7d__2f__7b_password_7d__20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__GET_20__2f_login_2f__7b_username_7d__2f__7b_password_7d__20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__GET_20__2f_login_2f__7b_username_7d__2f__7b_password_7d__20_Response_20_payloads__choicetype
		deleted;
}
creator.choicetype
qml_wadl_import__POST_20__2f_requestLoan_20_Response_20_payloads__choicetype
"POST /requestLoan Response payloads"
	deleted
{
	creator.structuredfield
	qml_wadl_import__POST_20__2f_requestLoan_20_Response_20_payloads__structuredfield__application_2f_xml
	"application/xml"
		type = qml_wadl_import__loanResponse__sequencetype
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_requestLoan_20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}
creator.choicetype
qml_wadl_import__POST_20__2f_withdraw_20_Response_20_payloads__choicetype
"POST /withdraw Response payloads"
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_withdraw_20_Response_20_payloads__primitivefield__application_2f_xml
	"application/xml"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_withdraw_20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}
creator.choicetype
qml_wadl_import__GET_20__2f_positions_2f__7b_positionId_7d__20_Response_20_payloads__choicetype
"GET /positions/{positionId} Response payloads"
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_positions_2f__7b_positionId_7d__20_Response_20_payloads__primitivefield__application_2f_xml
	"application/xml"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_positions_2f__7b_positionId_7d__20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}
creator.choicetype
qml_wadl_import__GET_20__2f_positions_2f__7b_positionId_7d__2f__7b_startDate_7d__2f__7b_endDate_7d__20_Response_20_payloads__choicetype
"GET /positions/{positionId}/{startDate}/{endDate} Response payloads"
	deleted
{
	creator.structuredfield
	qml_wadl_import__GET_20__2f_positions_2f__7b_positionId_7d__2f__7b_startDate_7d__2f__7b_endDate_7d__20_Response_20_payloads__structuredfield__application_2f_xml
	"application/xml"
		type =
	qml_wadl_import__Anonymous_20_sequence_20_in_20_top_20_level_20_element_20_historyPoints__sequencetype
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_positions_2f__7b_positionId_7d__2f__7b_startDate_7d__2f__7b_endDate_7d__20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__20_Response__message
"GET /customers/{customerId} Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__20_Response_20_payloads__choicetype
		deleted;
}
creator.choicetype
qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_month_2f__7b_month_7d__2f_type_2f__7b_type_7d__20_Response_20_payloads__choicetype
"GET /accounts/{accountId}/transactions/month/{month}/type/{type} Response payloads"
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_month_2f__7b_month_7d__2f_type_2f__7b_type_7d__20_Response_20_payloads__primitivefield__application_2f_xml
	"application/xml"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_month_2f__7b_month_7d__2f_type_2f__7b_type_7d__20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}
creator.sequencetype qml_wadl_import__loanResponse__sequencetype
"loanResponse"
	namespace = "http://service.parabank.parasoft.com/"
	deleted
{
	creator.primitivefield
	qml_wadl_import__loanResponse__primitivefield__responseDate "responseDate"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__loanResponse__primitivefield__loanProviderName
	"loanProviderName"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__loanResponse__primitivefield__approved "approved"
		type = boolean
		deleted;
	creator.primitivefield qml_wadl_import__loanResponse__primitivefield__message
	"message"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__loanResponse__primitivefield__accountId "accountId"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = number
		optional
		deleted;
}
creator.message
qml_wadl_import__POST_20__2f_createAccount_3f_customerId_3d__7b_customerId_7d__26_newAccountType_3d__7b_newAccountType_7d__26_fromAccountId_3d__7b_fromAccountId_7d__20_Request__message
"POST /createAccount?customerId={customerId}&newAccountType={newAccountType}&fromAccountId={fromAccountId} Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_createAccount_3f_customerId_3d__7b_customerId_7d__26_newAccountType_3d__7b_newAccountType_7d__26_fromAccountId_3d__7b_fromAccountId_7d__20_Request__primitivefield__customerId
	"customerId"
		type = number
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_createAccount_3f_customerId_3d__7b_customerId_7d__26_newAccountType_3d__7b_newAccountType_7d__26_fromAccountId_3d__7b_fromAccountId_7d__20_Request__primitivefield__newAccountType
	"newAccountType"
		type = number
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_createAccount_3f_customerId_3d__7b_customerId_7d__26_newAccountType_3d__7b_newAccountType_7d__26_fromAccountId_3d__7b_fromAccountId_7d__20_Request__primitivefield__fromAccountId
	"fromAccountId"
		type = number
		optional
		deleted;
}
creator.choicetype
qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__2f_positions_20_Response_20_payloads__choicetype
"GET /customers/{customerId}/positions Response payloads"
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__2f_positions_20_Response_20_payloads__primitivefield__application_2f_xml
	"application/xml"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__2f_positions_20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_login_2f__7b_username_7d__2f__7b_password_7d__20_Request__message
"GET /login/{username}/{password} Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_login_2f__7b_username_7d__2f__7b_password_7d__20_Request__primitivefield__username
	"username"
		type = String
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_login_2f__7b_username_7d__2f__7b_password_7d__20_Request__primitivefield__password
	"password"
		type = String
		optional
		deleted;
}
creator.choicetype
qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__20_Response_20_payloads__choicetype
"GET /accounts/{accountId} Response payloads"
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__20_Response_20_payloads__primitivefield__application_2f_xml
	"application/xml"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}
creator.choicetype
qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_buyPosition_20_Response_20_payloads__choicetype
"POST /customers/{customerId}/buyPosition Response payloads"
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_buyPosition_20_Response_20_payloads__primitivefield__application_2f_xml
	"application/xml"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_buyPosition_20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_transactions_2f__7b_transactionId_7d__20_Response__message
"GET /transactions/{transactionId} Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_transactions_2f__7b_transactionId_7d__20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__GET_20__2f_transactions_2f__7b_transactionId_7d__20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__GET_20__2f_transactions_2f__7b_transactionId_7d__20_Response_20_payloads__choicetype
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_onDate_2f__7b_onDate_7d__20_Request__message
"GET /accounts/{accountId}/transactions/onDate/{onDate} Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_onDate_2f__7b_onDate_7d__20_Request__primitivefield__accountId
	"accountId"
		type = number
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_onDate_2f__7b_onDate_7d__20_Request__primitivefield__onDate
	"onDate"
		type = String
		optional
		deleted;
}
creator.message
qml_wadl_import__POST_20__2f_customers_2f_update_2f__7b_customerId_7d__3f_firstName_3d__7b_firstName_7d__26_lastName_3d__7b_lastName_7d__26_street_3d__7b_street_7d__26_city_3d__7b_city_7d__26_state_3d__7b_state_7d__26_zipCode_3d__7b_zipCode_7d__26_phoneNumber_3d__7b_phoneNumber_7d__26_ssn_3d__7b_ssn_7d__26_username_3d__7b_username_7d__26_password_3d__7b_password_7d__20_Request__message
"POST /customers/update/{customerId}?firstName={firstName}&lastName={lastName}&street={street}&city={city}&state={state}&zipCode={zipCode}&phoneNumber={phoneNumber}&ssn={ssn}&username={username}&password={password} Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f_update_2f__7b_customerId_7d__3f_firstName_3d__7b_firstName_7d__26_lastName_3d__7b_lastName_7d__26_street_3d__7b_street_7d__26_city_3d__7b_city_7d__26_state_3d__7b_state_7d__26_zipCode_3d__7b_zipCode_7d__26_phoneNumber_3d__7b_phoneNumber_7d__26_ssn_3d__7b_ssn_7d__26_username_3d__7b_username_7d__26_password_3d__7b_password_7d__20_Request__primitivefield__customerId
	"customerId"
		type = number
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f_update_2f__7b_customerId_7d__3f_firstName_3d__7b_firstName_7d__26_lastName_3d__7b_lastName_7d__26_street_3d__7b_street_7d__26_city_3d__7b_city_7d__26_state_3d__7b_state_7d__26_zipCode_3d__7b_zipCode_7d__26_phoneNumber_3d__7b_phoneNumber_7d__26_ssn_3d__7b_ssn_7d__26_username_3d__7b_username_7d__26_password_3d__7b_password_7d__20_Request__primitivefield__firstName
	"firstName"
		type = String
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f_update_2f__7b_customerId_7d__3f_firstName_3d__7b_firstName_7d__26_lastName_3d__7b_lastName_7d__26_street_3d__7b_street_7d__26_city_3d__7b_city_7d__26_state_3d__7b_state_7d__26_zipCode_3d__7b_zipCode_7d__26_phoneNumber_3d__7b_phoneNumber_7d__26_ssn_3d__7b_ssn_7d__26_username_3d__7b_username_7d__26_password_3d__7b_password_7d__20_Request__primitivefield__lastName
	"lastName"
		type = String
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f_update_2f__7b_customerId_7d__3f_firstName_3d__7b_firstName_7d__26_lastName_3d__7b_lastName_7d__26_street_3d__7b_street_7d__26_city_3d__7b_city_7d__26_state_3d__7b_state_7d__26_zipCode_3d__7b_zipCode_7d__26_phoneNumber_3d__7b_phoneNumber_7d__26_ssn_3d__7b_ssn_7d__26_username_3d__7b_username_7d__26_password_3d__7b_password_7d__20_Request__primitivefield__street
	"street"
		type = String
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f_update_2f__7b_customerId_7d__3f_firstName_3d__7b_firstName_7d__26_lastName_3d__7b_lastName_7d__26_street_3d__7b_street_7d__26_city_3d__7b_city_7d__26_state_3d__7b_state_7d__26_zipCode_3d__7b_zipCode_7d__26_phoneNumber_3d__7b_phoneNumber_7d__26_ssn_3d__7b_ssn_7d__26_username_3d__7b_username_7d__26_password_3d__7b_password_7d__20_Request__primitivefield__city
	"city"
		type = String
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f_update_2f__7b_customerId_7d__3f_firstName_3d__7b_firstName_7d__26_lastName_3d__7b_lastName_7d__26_street_3d__7b_street_7d__26_city_3d__7b_city_7d__26_state_3d__7b_state_7d__26_zipCode_3d__7b_zipCode_7d__26_phoneNumber_3d__7b_phoneNumber_7d__26_ssn_3d__7b_ssn_7d__26_username_3d__7b_username_7d__26_password_3d__7b_password_7d__20_Request__primitivefield__state
	"state"
		type = String
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f_update_2f__7b_customerId_7d__3f_firstName_3d__7b_firstName_7d__26_lastName_3d__7b_lastName_7d__26_street_3d__7b_street_7d__26_city_3d__7b_city_7d__26_state_3d__7b_state_7d__26_zipCode_3d__7b_zipCode_7d__26_phoneNumber_3d__7b_phoneNumber_7d__26_ssn_3d__7b_ssn_7d__26_username_3d__7b_username_7d__26_password_3d__7b_password_7d__20_Request__primitivefield__zipCode
	"zipCode"
		type = String
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f_update_2f__7b_customerId_7d__3f_firstName_3d__7b_firstName_7d__26_lastName_3d__7b_lastName_7d__26_street_3d__7b_street_7d__26_city_3d__7b_city_7d__26_state_3d__7b_state_7d__26_zipCode_3d__7b_zipCode_7d__26_phoneNumber_3d__7b_phoneNumber_7d__26_ssn_3d__7b_ssn_7d__26_username_3d__7b_username_7d__26_password_3d__7b_password_7d__20_Request__primitivefield__phoneNumber
	"phoneNumber"
		type = String
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f_update_2f__7b_customerId_7d__3f_firstName_3d__7b_firstName_7d__26_lastName_3d__7b_lastName_7d__26_street_3d__7b_street_7d__26_city_3d__7b_city_7d__26_state_3d__7b_state_7d__26_zipCode_3d__7b_zipCode_7d__26_phoneNumber_3d__7b_phoneNumber_7d__26_ssn_3d__7b_ssn_7d__26_username_3d__7b_username_7d__26_password_3d__7b_password_7d__20_Request__primitivefield__ssn
	"ssn"
		type = String
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f_update_2f__7b_customerId_7d__3f_firstName_3d__7b_firstName_7d__26_lastName_3d__7b_lastName_7d__26_street_3d__7b_street_7d__26_city_3d__7b_city_7d__26_state_3d__7b_state_7d__26_zipCode_3d__7b_zipCode_7d__26_phoneNumber_3d__7b_phoneNumber_7d__26_ssn_3d__7b_ssn_7d__26_username_3d__7b_username_7d__26_password_3d__7b_password_7d__20_Request__primitivefield__username
	"username"
		type = String
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f_update_2f__7b_customerId_7d__3f_firstName_3d__7b_firstName_7d__26_lastName_3d__7b_lastName_7d__26_street_3d__7b_street_7d__26_city_3d__7b_city_7d__26_state_3d__7b_state_7d__26_zipCode_3d__7b_zipCode_7d__26_phoneNumber_3d__7b_phoneNumber_7d__26_ssn_3d__7b_ssn_7d__26_username_3d__7b_username_7d__26_password_3d__7b_password_7d__20_Request__primitivefield__password
	"password"
		type = String
		optional
		deleted;
}
creator.externalinterface
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
"https://parabank.parasoft.com/parabank/services/bank response"
	direction = out
	deleted;
creator.message
qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_sellPosition_20_Response__message
"POST /customers/{customerId}/sellPosition Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_sellPosition_20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_sellPosition_20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_sellPosition_20_Response_20_payloads__choicetype
		deleted;
}
creator.message qml_wadl_import__POST_20__2f_cleanDB_20_Request__message
"POST /cleanDB Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
}
creator.message
qml_wadl_import__POST_20__2f_billpay_3f_accountId_3d__7b_accountId_7d__26_amount_3d__7b_amount_7d__20_Request__message
"POST /billpay?accountId={accountId}&amount={amount} Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_billpay_3f_accountId_3d__7b_accountId_7d__26_amount_3d__7b_amount_7d__20_Request__primitivefield__accountId
	"accountId"
		type = number
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_billpay_3f_accountId_3d__7b_accountId_7d__26_amount_3d__7b_amount_7d__20_Request__primitivefield__amount
	"amount"
		type = String
		optional
		deleted;
	creator.structuredfield
	qml_wadl_import__POST_20__2f_billpay_3f_accountId_3d__7b_accountId_7d__26_amount_3d__7b_amount_7d__20_Request__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__POST_20__2f_billpay_3f_accountId_3d__7b_accountId_7d__26_amount_3d__7b_amount_7d__20_Request_20_payloads__choicetype
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_month_2f__7b_month_7d__2f_type_2f__7b_type_7d__20_Response__message
"GET /accounts/{accountId}/transactions/month/{month}/type/{type} Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_month_2f__7b_month_7d__2f_type_2f__7b_type_7d__20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_month_2f__7b_month_7d__2f_type_2f__7b_type_7d__20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_month_2f__7b_month_7d__2f_type_2f__7b_type_7d__20_Response_20_payloads__choicetype
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_20_Response__message
"GET /accounts/{accountId}/transactions Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_20_Response_20_payloads__choicetype
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__2f_positions_20_Request__message
"GET /customers/{customerId}/positions Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__2f_positions_20_Request__primitivefield__customerId
	"customerId"
		type = number
		optional
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_positions_2f__7b_positionId_7d__2f__7b_startDate_7d__2f__7b_endDate_7d__20_Response__message
"GET /positions/{positionId}/{startDate}/{endDate} Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_positions_2f__7b_positionId_7d__2f__7b_startDate_7d__2f__7b_endDate_7d__20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__GET_20__2f_positions_2f__7b_positionId_7d__2f__7b_startDate_7d__2f__7b_endDate_7d__20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__GET_20__2f_positions_2f__7b_positionId_7d__2f__7b_startDate_7d__2f__7b_endDate_7d__20_Response_20_payloads__choicetype
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_positions_2f__7b_positionId_7d__20_Response__message
"GET /positions/{positionId} Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_positions_2f__7b_positionId_7d__20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__GET_20__2f_positions_2f__7b_positionId_7d__20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__GET_20__2f_positions_2f__7b_positionId_7d__20_Response_20_payloads__choicetype
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_transactions_2f__7b_transactionId_7d__20_Request__message
"GET /transactions/{transactionId} Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_transactions_2f__7b_transactionId_7d__20_Request__primitivefield__transactionId
	"transactionId"
		type = number
		optional
		deleted;
}
creator.message
qml_wadl_import__POST_20__2f_customers_2f_update_2f__7b_customerId_7d__20_Response__message
"POST /customers/update/{customerId} Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f_update_2f__7b_customerId_7d__20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__POST_20__2f_customers_2f_update_2f__7b_customerId_7d__20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__POST_20__2f_customers_2f_update_2f__7b_customerId_7d__20_Response_20_payloads__choicetype
		deleted;
}
creator.message qml_wadl_import__POST_20__2f_withdraw_20_Response__message
"POST /withdraw Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_withdraw_20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__POST_20__2f_withdraw_20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__POST_20__2f_withdraw_20_Response_20_payloads__choicetype
		deleted;
}
creator.message
qml_wadl_import__POST_20__2f_setParameter_2f__7b_name_7d__2f__7b_value_7d__20_Response_20__5b_204_5d___message
"POST /setParameter/{name}/{value} Response [204]"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_setParameter_2f__7b_name_7d__2f__7b_value_7d__20_Response_20__5b_204_5d___primitivefield__status
	"status"
		type = number
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_positions_2f__7b_positionId_7d__2f__7b_startDate_7d__2f__7b_endDate_7d__20_Request__message
"GET /positions/{positionId}/{startDate}/{endDate} Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_positions_2f__7b_positionId_7d__2f__7b_startDate_7d__2f__7b_endDate_7d__20_Request__primitivefield__positionId
	"positionId"
		type = number
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_positions_2f__7b_positionId_7d__2f__7b_startDate_7d__2f__7b_endDate_7d__20_Request__primitivefield__startDate
	"startDate"
		type = String
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_positions_2f__7b_positionId_7d__2f__7b_startDate_7d__2f__7b_endDate_7d__20_Request__primitivefield__endDate
	"endDate"
		type = String
		optional
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__2f_positions_20_Response__message
"GET /customers/{customerId}/positions Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__2f_positions_20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__2f_positions_20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__2f_positions_20_Response_20_payloads__choicetype
		deleted;
}
creator.message qml_wadl_import__POST_20__2f_deposit_20_Response__message
"POST /deposit Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_deposit_20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__POST_20__2f_deposit_20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__POST_20__2f_deposit_20_Response_20_payloads__choicetype
		deleted;
}
creator.choicetype
qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__2f_accounts_20_Response_20_payloads__choicetype
"GET /customers/{customerId}/accounts Response payloads"
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__2f_accounts_20_Response_20_payloads__primitivefield__application_2f_xml
	"application/xml"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__2f_accounts_20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_20_Request__message
"GET /accounts/{accountId}/transactions Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_20_Request__primitivefield__accountId
	"accountId"
		type = number
		optional
		deleted;
}
creator.message
qml_wadl_import__POST_20__2f_requestLoan_3f_customerId_3d__7b_customerId_7d__26_amount_3d__7b_amount_7d__26_downPayment_3d__7b_downPayment_7d__26_fromAccountId_3d__7b_fromAccountId_7d__20_Request__message
"POST /requestLoan?customerId={customerId}&amount={amount}&downPayment={downPayment}&fromAccountId={fromAccountId} Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_requestLoan_3f_customerId_3d__7b_customerId_7d__26_amount_3d__7b_amount_7d__26_downPayment_3d__7b_downPayment_7d__26_fromAccountId_3d__7b_fromAccountId_7d__20_Request__primitivefield__customerId
	"customerId"
		type = number
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_requestLoan_3f_customerId_3d__7b_customerId_7d__26_amount_3d__7b_amount_7d__26_downPayment_3d__7b_downPayment_7d__26_fromAccountId_3d__7b_fromAccountId_7d__20_Request__primitivefield__amount
	"amount"
		type = String
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_requestLoan_3f_customerId_3d__7b_customerId_7d__26_amount_3d__7b_amount_7d__26_downPayment_3d__7b_downPayment_7d__26_fromAccountId_3d__7b_fromAccountId_7d__20_Request__primitivefield__downPayment
	"downPayment"
		type = String
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_requestLoan_3f_customerId_3d__7b_customerId_7d__26_amount_3d__7b_amount_7d__26_downPayment_3d__7b_downPayment_7d__26_fromAccountId_3d__7b_fromAccountId_7d__20_Request__primitivefield__fromAccountId
	"fromAccountId"
		type = number
		optional
		deleted;
}
creator.externalinterface
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
"https://parabank.parasoft.com/parabank/services/bank request"
	direction = in
	deleted;
creator.message
qml_wadl_import__POST_20__2f_startupJmsListener_20_Request__message
"POST /startupJmsListener Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
}
creator.choicetype
qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__20_Response_20_payloads__choicetype
"GET /customers/{customerId} Response payloads"
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__20_Response_20_payloads__primitivefield__application_2f_xml
	"application/xml"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}
creator.choicetype
qml_wadl_import__GET_20__2f_transactions_2f__7b_transactionId_7d__20_Response_20_payloads__choicetype
"GET /transactions/{transactionId} Response payloads"
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_transactions_2f__7b_transactionId_7d__20_Response_20_payloads__primitivefield__application_2f_xml
	"application/xml"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_transactions_2f__7b_transactionId_7d__20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_positions_2f__7b_positionId_7d__20_Request__message
"GET /positions/{positionId} Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_positions_2f__7b_positionId_7d__20_Request__primitivefield__positionId
	"positionId"
		type = number
		optional
		deleted;
}
creator.choicetype
qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_fromDate_2f__7b_fromDate_7d__2f_toDate_2f__7b_toDate_7d__20_Response_20_payloads__choicetype
"GET /accounts/{accountId}/transactions/fromDate/{fromDate}/toDate/{toDate} Response payloads"
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_fromDate_2f__7b_fromDate_7d__2f_toDate_2f__7b_toDate_7d__20_Response_20_payloads__primitivefield__application_2f_xml
	"application/xml"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_fromDate_2f__7b_fromDate_7d__2f_toDate_2f__7b_toDate_7d__20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__20_Response__message
"GET /accounts/{accountId} Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__20_Response_20_payloads__choicetype
		deleted;
}
creator.message qml_wadl_import__POST_20__2f_initializeDB_20_Request__message
"POST /initializeDB Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
}
creator.message
qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_buyPosition_20_Response__message
"POST /customers/{customerId}/buyPosition Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_buyPosition_20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_buyPosition_20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_buyPosition_20_Response_20_payloads__choicetype
		deleted;
}
creator.choicetype
qml_wadl_import__POST_20__2f_deposit_20_Response_20_payloads__choicetype
"POST /deposit Response payloads"
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_deposit_20_Response_20_payloads__primitivefield__application_2f_xml
	"application/xml"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_deposit_20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}
creator.sequencetype qml_wadl_import__historyPoint__sequencetype
"historyPoint"
	namespace = "http://service.parabank.parasoft.com/"
	deleted
{
	creator.primitivefield qml_wadl_import__historyPoint__primitivefield__symbol
	"symbol"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional
		deleted;
	creator.primitivefield qml_wadl_import__historyPoint__primitivefield__date
	"date"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__historyPoint__primitivefield__closingPrice "closingPrice"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = String
		optional
		deleted;
}
creator.message qml_wadl_import__POST_20__2f_requestLoan_20_Response__message
"POST /requestLoan Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_requestLoan_20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__POST_20__2f_requestLoan_20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__POST_20__2f_requestLoan_20_Response_20_payloads__choicetype
		deleted;
}
creator.choicetype
qml_wadl_import__POST_20__2f_billpay_20_Response_20_payloads__choicetype
"POST /billpay Response payloads"
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_billpay_20_Response_20_payloads__primitivefield__application_2f_xml
	"application/xml"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_billpay_20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}
creator.choicetype
qml_wadl_import__POST_20__2f_customers_2f_update_2f__7b_customerId_7d__20_Response_20_payloads__choicetype
"POST /customers/update/{customerId} Response payloads"
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f_update_2f__7b_customerId_7d__20_Response_20_payloads__primitivefield__application_2f_xml
	"application/xml"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f_update_2f__7b_customerId_7d__20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}
creator.message
qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_buyPosition_3f_accountId_3d__7b_accountId_7d__26_name_3d__7b_name_7d__26_symbol_3d__7b_symbol_7d__26_shares_3d__7b_shares_7d__26_pricePerShare_3d__7b_pricePerShare_7d__20_Request__message
"POST /customers/{customerId}/buyPosition?accountId={accountId}&name={name}&symbol={symbol}&shares={shares}&pricePerShare={pricePerShare} Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_buyPosition_3f_accountId_3d__7b_accountId_7d__26_name_3d__7b_name_7d__26_symbol_3d__7b_symbol_7d__26_shares_3d__7b_shares_7d__26_pricePerShare_3d__7b_pricePerShare_7d__20_Request__primitivefield__customerId
	"customerId"
		type = number
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_buyPosition_3f_accountId_3d__7b_accountId_7d__26_name_3d__7b_name_7d__26_symbol_3d__7b_symbol_7d__26_shares_3d__7b_shares_7d__26_pricePerShare_3d__7b_pricePerShare_7d__20_Request__primitivefield__accountId
	"accountId"
		type = number
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_buyPosition_3f_accountId_3d__7b_accountId_7d__26_name_3d__7b_name_7d__26_symbol_3d__7b_symbol_7d__26_shares_3d__7b_shares_7d__26_pricePerShare_3d__7b_pricePerShare_7d__20_Request__primitivefield__name
	"name"
		type = String
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_buyPosition_3f_accountId_3d__7b_accountId_7d__26_name_3d__7b_name_7d__26_symbol_3d__7b_symbol_7d__26_shares_3d__7b_shares_7d__26_pricePerShare_3d__7b_pricePerShare_7d__20_Request__primitivefield__symbol
	"symbol"
		type = String
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_buyPosition_3f_accountId_3d__7b_accountId_7d__26_name_3d__7b_name_7d__26_symbol_3d__7b_symbol_7d__26_shares_3d__7b_shares_7d__26_pricePerShare_3d__7b_pricePerShare_7d__20_Request__primitivefield__shares
	"shares"
		type = number
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_buyPosition_3f_accountId_3d__7b_accountId_7d__26_name_3d__7b_name_7d__26_symbol_3d__7b_symbol_7d__26_shares_3d__7b_shares_7d__26_pricePerShare_3d__7b_pricePerShare_7d__20_Request__primitivefield__pricePerShare
	"pricePerShare"
		type = String
		optional
		deleted;
}
creator.choicetype
qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_sellPosition_20_Response_20_payloads__choicetype
"POST /customers/{customerId}/sellPosition Response payloads"
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_sellPosition_20_Response_20_payloads__primitivefield__application_2f_xml
	"application/xml"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_customers_2f__7b_customerId_7d__2f_sellPosition_20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}
creator.message
qml_wadl_import__POST_20__2f_shutdownJmsListener_20_Request__message
"POST /shutdownJmsListener Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
}
creator.sequencetype
qml_wadl_import__Anonymous_20_sequence_20_in_20_top_20_level_20_element_20_historyPoints__sequencetype
"Anonymous sequence in top level element historyPoints"
	namespace = "http://service.parabank.parasoft.com/"
	deleted
{
	creator.structuredfield
	qml_wadl_import__Anonymous_20_sequence_20_in_20_top_20_level_20_element_20_historyPoints__structuredfield__historyPoint
	"historyPoint"
		namespace = "http://service.parabank.parasoft.com/"
		annotations = [ "xsd:minOccurs" = "0";
	]
		type = qml_wadl_import__historyPoint__sequencetype array
		deleted;
}
creator.message
qml_wadl_import__POST_20__2f_transfer_3f_fromAccountId_3d__7b_fromAccountId_7d__26_toAccountId_3d__7b_toAccountId_7d__26_amount_3d__7b_amount_7d__20_Request__message
"POST /transfer?fromAccountId={fromAccountId}&toAccountId={toAccountId}&amount={amount} Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_transfer_3f_fromAccountId_3d__7b_fromAccountId_7d__26_toAccountId_3d__7b_toAccountId_7d__26_amount_3d__7b_amount_7d__20_Request__primitivefield__fromAccountId
	"fromAccountId"
		type = number
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_transfer_3f_fromAccountId_3d__7b_fromAccountId_7d__26_toAccountId_3d__7b_toAccountId_7d__26_amount_3d__7b_amount_7d__20_Request__primitivefield__toAccountId
	"toAccountId"
		type = number
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_transfer_3f_fromAccountId_3d__7b_fromAccountId_7d__26_toAccountId_3d__7b_toAccountId_7d__26_amount_3d__7b_amount_7d__20_Request__primitivefield__amount
	"amount"
		type = String
		optional
		deleted;
}
creator.message
qml_wadl_import__POST_20__2f_deposit_3f_accountId_3d__7b_accountId_7d__26_amount_3d__7b_amount_7d__20_Request__message
"POST /deposit?accountId={accountId}&amount={amount} Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_deposit_3f_accountId_3d__7b_accountId_7d__26_amount_3d__7b_amount_7d__20_Request__primitivefield__accountId
	"accountId"
		type = number
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__POST_20__2f_deposit_3f_accountId_3d__7b_accountId_7d__26_amount_3d__7b_amount_7d__20_Request__primitivefield__amount
	"amount"
		type = String
		optional
		deleted;
}
creator.message
qml_wadl_import__POST_20__2f_shutdownJmsListener_20_Response_20__5b_204_5d___message
"POST /shutdownJmsListener Response [204]"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__POST_20__2f_shutdownJmsListener_20_Response_20__5b_204_5d___primitivefield__status
	"status"
		type = number
		deleted;
}
creator.choicetype
qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_20_Response_20_payloads__choicetype
"GET /accounts/{accountId}/transactions Response payloads"
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_20_Response_20_payloads__primitivefield__application_2f_xml
	"application/xml"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_month_2f__7b_month_7d__2f_type_2f__7b_type_7d__20_Request__message
"GET /accounts/{accountId}/transactions/month/{month}/type/{type} Request"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_request__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_month_2f__7b_month_7d__2f_type_2f__7b_type_7d__20_Request__primitivefield__accountId
	"accountId"
		type = number
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_month_2f__7b_month_7d__2f_type_2f__7b_type_7d__20_Request__primitivefield__month
	"month"
		type = String
		optional
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_month_2f__7b_month_7d__2f_type_2f__7b_type_7d__20_Request__primitivefield__type
	"type"
		type = String
		optional
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__2f_accounts_20_Response__message
"GET /customers/{customerId}/accounts Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__2f_accounts_20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__2f_accounts_20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__GET_20__2f_customers_2f__7b_customerId_7d__2f_accounts_20_Response_20_payloads__choicetype
		deleted;
}
creator.message
qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_fromDate_2f__7b_fromDate_7d__2f_toDate_2f__7b_toDate_7d__20_Response__message
"GET /accounts/{accountId}/transactions/fromDate/{fromDate}/toDate/{toDate} Response"
	interfaces = [
qml_wadl_import__https_3a__2f__2f_parabank_2e_parasoft_2e_com_2f_parabank_2f_services_2f_bank_20_response__externalinterface
	deleted ]
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_fromDate_2f__7b_fromDate_7d__2f_toDate_2f__7b_toDate_7d__20_Response__primitivefield__status
	"status"
		type = number
		deleted;
	creator.structuredfield
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_fromDate_2f__7b_fromDate_7d__2f_toDate_2f__7b_toDate_7d__20_Response__structuredfield__payload
	"payload"
		annotations = [ "http:contenttype" = "application/json";
	]
		type =
	qml_wadl_import__GET_20__2f_accounts_2f__7b_accountId_7d__2f_transactions_2f_fromDate_2f__7b_fromDate_7d__2f_toDate_2f__7b_toDate_7d__20_Response_20_payloads__choicetype
		deleted;
}
creator.choicetype
qml_wadl_import__GET_20__2f_login_2f__7b_username_7d__2f__7b_password_7d__20_Response_20_payloads__choicetype
"GET /login/{username}/{password} Response payloads"
	deleted
{
	creator.primitivefield
	qml_wadl_import__GET_20__2f_login_2f__7b_username_7d__2f__7b_password_7d__20_Response_20_payloads__primitivefield__application_2f_xml
	"application/xml"
		type = String
		deleted;
	creator.primitivefield
	qml_wadl_import__GET_20__2f_login_2f__7b_username_7d__2f__7b_password_7d__20_Response_20_payloads__primitivefield__application_2f_json
	"application/json"
		type = String
		deleted;
}