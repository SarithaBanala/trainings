package testsuite;
import org.testng.annotations.Test;
import PageObjects.*;
import utilities.PageObjectBase;
import org.openqa.selenium.support.PageFactory;
import utilities.Configurations;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import java.util.HashMap;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import utilities.TestReport;
import java.io.IOException;
import org.testng.Reporter;
import utilities.DataUtil;


/** Conformiq generated test case
	Project_X_Sprint_Y_1
*/
public class Project_X_Sprint_Y_1 extends PageObjectBase
{

	public Project_X_Sprint_Y_1()
	{
	}

	private TestReport testReport= new TestReport();


	private StringBuilder overallTestData= new StringBuilder();


	@Test(dataProvider="TestData")
	public void test(final String Step_1_Login_BUTTON_Status,final String Step_1_Username_TEXTBOX_Status,final String Step_1_Username_TEXTBOX_Verification,final String Step_1_Password_TEXTBOX_Status,final String Step_1_Password_TEXTBOX_Verification,final String Step_1_Status_LABEL_Status,final String Step_2_Username_TEXTBOX,final String Step_2_Password_TEXTBOX,final String Step_4_Login_BUTTON_Status,final String Step_4_Username_TEXTBOX_Status,final String Step_4_Username_TEXTBOX_Verification,final String Step_4_Password_TEXTBOX_Status,final String Step_4_Password_TEXTBOX_Verification,final String Step_4_Status_LABEL_Status) throws Exception

	{

	Allocate_car_Page allocate_car_page_init=PageFactory.initElements(driver, Allocate_car_Page.class);

	_verfication_Car_is_allocated_Page _verfication_car_is_allocated_page_init=PageFactory.initElements(driver, _verfication_Car_is_allocated_Page.class);

	_verfication_Request_Rejected_Page _verfication_request_rejected_page_init=PageFactory.initElements(driver, _verfication_Request_Rejected_Page.class);

	_verfication_Allocate_car_Page _verfication_allocate_car_page_init=PageFactory.initElements(driver, _verfication_Allocate_car_Page.class);

	_verfication_Review_Request_Page _verfication_review_request_page_init=PageFactory.initElements(driver, _verfication_Review_Request_Page.class);

	_verfication_Platform_Transportation_Page _verfication_platform_transportation_page_init=PageFactory.initElements(driver, _verfication_Platform_Transportation_Page.class);

	Platform_Transportation_Page platform_transportation_page_init=PageFactory.initElements(driver, Platform_Transportation_Page.class);

	_verfication_Register_request_Page _verfication_register_request_page_init=PageFactory.initElements(driver, _verfication_Register_request_Page.class);

	Register_request_Page register_request_page_init=PageFactory.initElements(driver, Register_request_Page.class);

	Car_is_allocated_Page car_is_allocated_page_init=PageFactory.initElements(driver, Car_is_allocated_Page.class);

	Is_Option__Page is_option__page_init=PageFactory.initElements(driver, Is_Option__Page.class);

	Review_Request_Page review_request_page_init=PageFactory.initElements(driver, Review_Request_Page.class);

	Request_Rejected_Page request_rejected_page_init=PageFactory.initElements(driver, Request_Rejected_Page.class);

	RightClick_Page rightclick_page_init=PageFactory.initElements(driver, RightClick_Page.class);

	Verify_Status_Page verify_status_page_init=PageFactory.initElements(driver, Verify_Status_Page.class);

	Screen_Page screen_page_init=PageFactory.initElements(driver, Screen_Page.class);

	Popup_Page popup_page_init=PageFactory.initElements(driver, Popup_Page.class);

	Login_Page login_page_init=PageFactory.initElements(driver, Login_Page.class);

	Home_Page home_page_init=PageFactory.initElements(driver, Home_Page.class);

	Open_New_Account_Page open_new_account_page_init=PageFactory.initElements(driver, Open_New_Account_Page.class);

	Transfer_Funds_Page transfer_funds_page_init=PageFactory.initElements(driver, Transfer_Funds_Page.class);

	User_Info_Request_Page user_info_request_page_init=PageFactory.initElements(driver, User_Info_Request_Page.class);

	Make_My_Trip_Page make_my_trip_page_init=PageFactory.initElements(driver, Make_My_Trip_Page.class);

	Order_deatils_Page order_deatils_page_init=PageFactory.initElements(driver, Order_deatils_Page.class);
	testReport.createTesthtmlHeader(overallTestData);

	testReport.createHead(overallTestData);

	testReport.putLogo(overallTestData);

	float ne = (float) 0.0;

	testReport.generateGeneralInfo(overallTestData, "Project_X_Sprint_Y_1", "TC_Project_X_Sprint_Y_1", "",ne);

	testReport.createStepHeader();

	//External Circumstances


	Reporter.log("Step - 1- verify Login screen");

	testReport.fillTableStep("Step 1", "verify Login screen");

	login_page_init.verify_Login_Status(Step_1_Login_BUTTON_Status);

	login_page_init.verify_Username_Status(Step_1_Username_TEXTBOX_Status);

	login_page_init.verify_Username(Step_1_Username_TEXTBOX_Verification);
	login_page_init.verify_Password_Status(Step_1_Password_TEXTBOX_Status);

	login_page_init.verify_Password(Step_1_Password_TEXTBOX_Verification);
	login_page_init.verify_Status_Status(Step_1_Status_LABEL_Status);

	getScreenshot(driver,Configurations.screenshotLocation , "Project_X_Sprint_Y_1","Step_1");

	Reporter.log("Step - 2- Fill Customer Login form Login screen");

	testReport.fillTableStep("Step 2", "Fill Customer Login form Login screen");

	login_page_init.set_Username(Step_2_Username_TEXTBOX);
	login_page_init.set_Password(Step_2_Password_TEXTBOX);
	getScreenshot(driver,Configurations.screenshotLocation , "Project_X_Sprint_Y_1","Step_2");

	Reporter.log("Step - 3- click Login button Login screen");

	testReport.fillTableStep("Step 3", "click Login button Login screen");

	login_page_init.click_Login();
	getScreenshot(driver,Configurations.screenshotLocation , "Project_X_Sprint_Y_1","Step_3");

	Reporter.log("Step - 4- verify Login screen");

	testReport.fillTableStep("Step 4", "verify Login screen");

	login_page_init.verify_Login_Status(Step_4_Login_BUTTON_Status);

	login_page_init.verify_Username_Status(Step_4_Username_TEXTBOX_Status);

	login_page_init.verify_Username(Step_4_Username_TEXTBOX_Verification);
	login_page_init.verify_Password_Status(Step_4_Password_TEXTBOX_Status);

	login_page_init.verify_Password(Step_4_Password_TEXTBOX_Verification);
	login_page_init.verify_Status_Status(Step_4_Status_LABEL_Status);

	getScreenshot(driver,Configurations.screenshotLocation , "Project_X_Sprint_Y_1","Step_4");
	}
	@DataProvider(name = "TestData")
	public Object[][] getData() {
	return DataUtil.getDataFromSpreadSheet("TestData.xlsx", "TCID_1");
}
	@AfterTest
	public void export(){
		testReport.appendtestData(overallTestData);
		testReport.closeStepTable();
		testReport.closeTestHTML(overallTestData);
		driver.close();
		try {
			testReport.writeTestReporthtml(overallTestData, "Project_X_Sprint_Y_1");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
