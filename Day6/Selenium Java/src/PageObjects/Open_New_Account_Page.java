package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Open_New_Account_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Account_Type")
	public static WebElement Account_Type;

public void verify_Account_Type(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Account_Type.getAttribute("value"),data);
	}

}

public void verify_Account_Type_Status(String data){
		//Verifies the Status of the Account_Type
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Account_Type.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Account_Type.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Account_Type.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Account_Type.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_Account_Type(String data){
		Select dropdown= new Select(Account_Type);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "Account_Number")
	public static WebElement Account_Number;

public void verify_Account_Number(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Account_Number.getAttribute("value"),data);
	}

}

public void verify_Account_Number_Status(String data){
		//Verifies the Status of the Account_Number
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Account_Number.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Account_Number.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Account_Number.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Account_Number.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_Account_Number(String data){
		Select dropdown= new Select(Account_Number);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "Open_New_Account")
	public static WebElement Open_New_Account;

public void verify_Open_New_Account_Status(String data){
		//Verifies the Status of the Open_New_Account
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Open_New_Account.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Open_New_Account.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Open_New_Account.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Open_New_Account.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Open_New_Account(){
		Open_New_Account.click();
}

@FindBy(how= How.ID, using = "Status")
	public static WebElement Status;

public void verify_Status(String data){
		Assert.assertEquals(Status,Status);
}

public void verify_Status_Status(String data){
		//Verifies the Status of the Status
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Status.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Status.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Status.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Status.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}