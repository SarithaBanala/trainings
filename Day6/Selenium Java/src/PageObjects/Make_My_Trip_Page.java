package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Make_My_Trip_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Flight_details")
	public static WebElement Flight_details;

public void verify_Flight_details(String data){
		Assert.assertEquals(Flight_details,Flight_details);
}

@FindBy(how= How.ID, using = "Select")
	public static WebElement Select;

public void verify_Select_Status(String data){
		//Verifies the Status of the Select
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Select.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Select.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Select.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Select.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Select(){
		Select.click();
}

@FindBy(how= How.ID, using = "Flight_Details")
	public static WebElement Flight_Details;

public void verify_Flight_Details_Status(String data){
		//Verifies the Status of the Flight_Details
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Flight_Details.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Flight_Details.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Flight_Details.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Flight_Details.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Flight_Details(){
		Flight_Details.click();
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}