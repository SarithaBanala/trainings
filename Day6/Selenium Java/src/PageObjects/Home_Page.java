package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Home_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Open_New_Account")
	public static WebElement Open_New_Account;

public void verify_Open_New_Account_Status(String data){
		//Verifies the Status of the Open_New_Account
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Open_New_Account.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Open_New_Account.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Open_New_Account.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Open_New_Account.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Open_New_Account(){
		Open_New_Account.click();
}

@FindBy(how= How.ID, using = "Transfer_Funds")
	public static WebElement Transfer_Funds;

public void verify_Transfer_Funds_Status(String data){
		//Verifies the Status of the Transfer_Funds
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Transfer_Funds.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Transfer_Funds.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Transfer_Funds.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Transfer_Funds.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Transfer_Funds(){
		Transfer_Funds.click();
}

@FindBy(how= How.ID, using = "Logout")
	public static WebElement Logout;

public void verify_Logout_Status(String data){
		//Verifies the Status of the Logout
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Logout.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Logout.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Logout.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Logout.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Logout(){
		Logout.click();
}

@FindBy(how= How.ID, using = "Status")
	public static WebElement Status;

public void verify_Status(String data){
		Assert.assertEquals(Status,Status);
}

public void verify_Status_Status(String data){
		//Verifies the Status of the Status
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Status.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Status.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Status.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Status.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}