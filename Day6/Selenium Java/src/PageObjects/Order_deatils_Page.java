package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Order_deatils_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "OrdName")
	public static WebElement OrdName;

public void verify_OrdName(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(OrdName.getAttribute("value"),data);
	}

}

public void verify_OrdName_Status(String data){
		//Verifies the Status of the OrdName
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(OrdName.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(OrdName.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!OrdName.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!OrdName.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_OrdName(String data){
		OrdName.clear();
		OrdName.sendKeys(data);
}

@FindBy(how= How.ID, using = "Qty")
	public static WebElement Qty;

public void verify_Qty(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Qty.getAttribute("value"),data);
	}

}

public void verify_Qty_Status(String data){
		//Verifies the Status of the Qty
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Qty.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Qty.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Qty.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Qty.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Qty(String data){
		Qty.clear();
		Qty.sendKeys(data);
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}