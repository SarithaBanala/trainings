package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class User_Info_Request_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Name")
	public static WebElement Name;

public void verify_Name(String data){
		Assert.assertEquals(Name,Name);
}

public void enter_Name(String data){
		Name.sendKeys(data);
}

@FindBy(how= How.ID, using = "Gender")
	public static WebElement Gender;

public void verify_Gender(String data){
		Assert.assertEquals(Gender,Gender);
}

public void enter_Gender(String data){
		Gender.sendKeys(data);
}

@FindBy(how= How.ID, using = "Address")
	public static WebElement Address;

public void verify_Address(String data){
		Assert.assertEquals(Address,Address);
}

public void enter_Address(String data){
		Address.sendKeys(data);
}

@FindBy(how= How.ID, using = "S_No")
	public static WebElement S_No;

public void verify_S_No(String data){
		Assert.assertEquals(S_No,S_No);
}

public void enter_S_No(String data){
		S_No.sendKeys(data);
}

@FindBy(how= How.ID, using = "City")
	public static WebElement City;

public void verify_City(String data){
		Assert.assertEquals(City,City);
}

public void enter_City(String data){
		City.sendKeys(data);
}

@FindBy(how= How.ID, using = "Country")
	public static WebElement Country;

public void verify_Country(String data){
		Assert.assertEquals(Country,Country);
}

public void enter_Country(String data){
		Country.sendKeys(data);
}

@FindBy(how= How.ID, using = "ZipCode")
	public static WebElement ZipCode;

public void verify_ZipCode(String data){
		Assert.assertEquals(ZipCode,ZipCode);
}

public void enter_ZipCode(String data){
		ZipCode.sendKeys(data);
}

@FindBy(how= How.ID, using = "Permanent_Address")
	public static WebElement Permanent_Address;

public void verify_Permanent_Address(String data){
		Assert.assertEquals(Permanent_Address,Permanent_Address);
}

public void enter_Permanent_Address(String data){
		Permanent_Address.sendKeys(data);
}

@FindBy(how= How.ID, using = "unnamed")
	public static WebElement unnamed;

public void verify_unnamed(String data){
		Assert.assertEquals(unnamed,unnamed);
}

public void enter_unnamed(String data){
		unnamed.sendKeys(data);
}

@FindBy(how= How.ID, using = "Temp_Address")
	public static WebElement Temp_Address;

public void verify_Temp_Address(String data){
		Assert.assertEquals(Temp_Address,Temp_Address);
}

public void enter_Temp_Address(String data){
		Temp_Address.sendKeys(data);
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}