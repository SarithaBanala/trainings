package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class RightClick_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Button")
	public static WebElement Button;

public void verify_Button(String data){
		Assert.assertEquals(Button,Button);
}

public void verify_Button_Status(String data){
		//Verifies the Status of the Button
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Button.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Button.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Button.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Button.isEnabled());
				break;
			default:
				break;
			}
		}
	}
@FindBy(how= How.ID, using = "Button_Name")
	public static WebElement Button_Name;

public void verify_Button_Name(String data){
		Assert.assertEquals(Button_Name,Button_Name);
}

public void enter_Button_Name(String data){
		Button_Name.sendKeys(data);
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}