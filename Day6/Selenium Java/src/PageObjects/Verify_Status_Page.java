package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Verify_Status_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Status")
	public static WebElement Status;

public void verify_Status(String data){
		Assert.assertEquals(Status,Status);
}

public void enter_Status(String data){
		Status.sendKeys(data);
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}