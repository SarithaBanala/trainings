package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Screen_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Btn")
	public static WebElement Btn;

public void verify_Btn_Status(String data){
		//Verifies the Status of the Btn
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Btn.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Btn.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Btn.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Btn.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Btn(){
		Btn.click();
}

@FindBy(how= How.ID, using = "Link")
	public static WebElement Link;

public void verify_Link_Status(String data){
		//Verifies the Status of the Link
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Link.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Link.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Link.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Link.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Link(){
		Link.click();
}

@FindBy(how= How.ID, using = "Label")
	public static WebElement Label;

public void verify_Label(String data){
		Assert.assertEquals(Label,Label);
}

public void verify_Label_Status(String data){
		//Verifies the Status of the Label
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Label.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Label.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Label.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Label.isEnabled());
				break;
			default:
				break;
			}
		}
	}
@FindBy(how= How.ID, using = "TB")
	public static WebElement TB;

public void verify_TB(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(TB.getAttribute("value"),data);
	}

}

public void verify_TB_Status(String data){
		//Verifies the Status of the TB
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(TB.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(TB.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!TB.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!TB.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_TB(String data){
		TB.clear();
		TB.sendKeys(data);
}

@FindBy(how= How.ID, using = "CB")
	public static WebElement CB;

public void verify_CB(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(CB.getAttribute("value"),data);
	}

}

public void verify_CB_Status(String data){
		//Verifies the Status of the CB
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(CB.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(CB.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!CB.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!CB.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_CB(String data){
		if(CB.isSelected());
			CB.click();
}

@FindBy(how= How.ID, using = "DD")
	public static WebElement DD;

public void verify_DD(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(DD.getAttribute("value"),data);
	}

}

public void verify_DD_Status(String data){
		//Verifies the Status of the DD
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(DD.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(DD.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!DD.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!DD.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_DD(String data){
		Select dropdown= new Select(DD);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "RB")
	public static WebElement RB;

public void verify_RB(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(RB.getAttribute("name"),data);
	}

}

public void verify_RB_Status(String data){
		//Verifies the Status of the RB
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(RB.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(RB.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!RB.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!RB.isEnabled());
				break;
			default:
				break;
			}
		}
	}
@FindBy(how= How.ID, using = "Calendar")
	public static WebElement Calendar;

public void verify_Calendar(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Calendar.getAttribute("value"),data);
	}

}

public void verify_Calendar_Status(String data){
		//Verifies the Status of the Calendar
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Calendar.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Calendar.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Calendar.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Calendar.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_Calendar(String data){
		Calendar.click();
}

@FindBy(how= How.ID, using = "LB")
	public static WebElement LB;

public void verify_LB(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(LB.getAttribute("value"),data);
	}

}

public void verify_LB_Status(String data){
		//Verifies the Status of the LB
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(LB.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(LB.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!LB.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!LB.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_LB(String data){
		LB.click();
}

@FindBy(how= How.ID, using = "Menus")
	public static WebElement Menus;

public void click_Menus(){
		Menus.click();
}

@FindBy(how= How.ID, using = "File")
	public static WebElement File;

public void click_File(){
		File.click();
}

@FindBy(how= How.ID, using = "Click")
	public static WebElement Click;

public void click_Click(){
		Click.click();
}

@FindBy(how= How.ID, using = "Mouse_over")
	public static WebElement Mouse_over;

public void click_Mouse_over(){
		Mouse_over.click();
}

@FindBy(how= How.ID, using = "SubMenus")
	public static WebElement SubMenus;

public void click_SubMenus(){
		SubMenus.click();
}

@FindBy(how= How.ID, using = "Modeling")
	public static WebElement Modeling;

public void click_Modeling(){
		Modeling.click();
}

@FindBy(how= How.ID, using = "Test_Design")
	public static WebElement Test_Design;

public void click_Test_Design(){
		Test_Design.click();
}

@FindBy(how= How.ID, using = "unnamed")
	public static WebElement unnamed;

public void click_unnamed(){
		unnamed.click();
}

@FindBy(how= How.ID, using = "Project_explorer")
	public static WebElement Project_explorer;

public void click_Project_explorer(){
		Project_explorer.click();
}

@FindBy(how= How.ID, using = "Day1")
	public static WebElement Day1;

public void click_Day1(){
		Day1.click();
}

@FindBy(how= How.ID, using = "DC")
	public static WebElement DC;

public void click_DC(){
		DC.click();
}

@FindBy(how= How.ID, using = "ExcelScripter")
	public static WebElement ExcelScripter;

public void click_ExcelScripter(){
		ExcelScripter.click();
}

@FindBy(how= How.ID, using = "ConformiQ_Options")
	public static WebElement ConformiQ_Options;

public void click_ConformiQ_Options(){
		ConformiQ_Options.click();
}

@FindBy(how= How.ID, using = "Requirement_Providers")
	public static WebElement Requirement_Providers;

public void click_Requirement_Providers(){
		Requirement_Providers.click();
}

@FindBy(how= How.ID, using = "Model")
	public static WebElement Model;

public void click_Model(){
		Model.click();
}

@FindBy(how= How.ID, using = "AD")
	public static WebElement AD;

public void click_AD(){
		AD.click();
}

@FindBy(how= How.ID, using = "SD")
	public static WebElement SD;

public void click_SD(){
		SD.click();
}

@FindBy(how= How.ID, using = "_sl")
	public static WebElement _sl;

public void click__sl(){
		_sl.click();
}

@FindBy(how= How.ID, using = "RB_F")
	public WebElement RB_F;

public void verify_RB_F_Status(String data){
		//Verifies the Status of the RB_F
		Assert.assertEquals(RB_F,RB_F);
}

public void select_RB_F(){
		RB_F.click();
}

@FindBy(how= How.ID, using = "RB_M")
	public WebElement RB_M;

public void verify_RB_M_Status(String data){
		//Verifies the Status of the RB_M
		Assert.assertEquals(RB_M,RB_M);
}

public void select_RB_M(){
		RB_M.click();
}

@FindBy(how= How.ID, using = "RB_Popup")
	public WebElement RB_Popup;

public void verify_RB_Popup_Status(String data){
		//Verifies the Status of the RB_Popup
		Assert.assertEquals(RB_Popup,RB_Popup);
}

public void select_RB_Popup(){
		RB_Popup.click();
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}